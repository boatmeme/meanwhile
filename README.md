![Meanwhile](https://bitbucket.org/boatmeme/meanwhile/raw/master/doc/images/MeanwhileLogo.png)
# A simple concurrency library for parallel task execution in Java

## What is it?
---

Meanwhile is a small Java library for performing arbitrary work concurrently. Configuration is minimal (one line), and creation of Tasks requires no more than the implementation of a single `execute()` method. 

There are two basic constructs to understand: The _**TaskQueueManager**_ and the _**Task**_.

### Task
---

_Task_ is an abstract class that encapsulates a single unit of work. The minimal amount of code to use Meanwhile in your application involves providing a concrete implementation of this class and @Overriding the `execute()` method. Here's a snippet of code that demonstrates an anonymous inner-class that prints the numbers 1 - 1000:

    TaskQueueManager.submit("MyNamedTaskQueue", new Task() {
        int countTo = 1000;

        @Override
        protected boolean execute() {
            for(int i = 1; i<=countTo; i++) {
               System.out.println(i);
            }
            return true;
        }
    });

_Tasks_ call a series of standard methods throughout their lifecycle. The default implementation of these Lifecycle methods merely update the _Task's_ _**TaskStatus**_, but you may @Override these methods to perform more sophisticated functionality. Broadcasting messages to a client over your application's existing messaging framework, is a common example. 

- **onSubmit()** - called immediately after the _Task_ has been queued by the _TaskQueueManager_
- **onStart()** - called just prior to execution of the _Task's_ `execute()` method
- **onSuccess()** - called immediately after `execute()`, but only if `execute()` returns a boolean true
- **onFailure()** - called immediately after `execute()`, if `execute()` raises an Exception or returns a boolean false
- **onComplete()** - the last Lifecycle method to be called. It is called regardless of whether the task succeeds or fails.
- **onCancel()** - this method is called if the _Task_ is removed from the _TaskQueueManager's_ queue prior to execution. `onCancel()` is also called during `onFailure()` of a task's dependencies (Chained composition). It will never execute after `onStart()`.

>__NOTE__: If you @Override any of the Lifecycle methods, it is good practice to call the `super` version of these methods inside of your implementation


### TaskQueueManager
---  

_TaskQueueManager_ is a static class that provides a single interface to individual _TaskQueues_ , allowing you to manage the queuing and execution of individual _Tasks_ . As of Meanwhile 0.3.1, you may configure multiple named queues per JVM.

To configure the default amount of threads available to new instances of Meanwhile's _TaskQueue_ class, set the following Java System property to the maximum number of concurrent _Tasks_ you wish to execute (per queue):

`meanwhile.task.threads = 10` (default)

Additionally, you can set the number of threads on a per queue basis by calling the following static method, and passing the queue name and number of threads as parameters:

`TaskQueueManager.setMaximumThreadCount("NameOfMyQueue", 10)`

There are two primary strategies for submitting _Tasks_ to the _TaskQueueManager_ for execution: Asynchronous and Synchronous.

#### Asynchronous

- __TaskQueueManager.submit(Task myTask)__ - calling this method submits your _Task_ for execution and immediately returns control to your code. Although it returns an instance of a Java __Future__ that can be used to block until the task is complete, most often `TaskQueueManager.submit()` will be called in a "Fire-and-Forget", asynchronous execution scenario.

![AsyncStrategy](https://bitbucket.org/boatmeme/meanwhile/raw/master/doc/images/Asynchronous_Strategy.png)

#### Synchronous

- __TaskQueueManager.blockingSubmit(List&lt;Task&gt; myTasks)__ - calling this method submits a list of all the _Task_s you would like executed and waits until all of those tasks have completed before returning control to the calling code. This strategy is commonly used in scenarios where you would like to parallelize some units of work - often in the context of a single service call - and then do something else with the results only after all of the work has completed.

![SyncStrategy](https://bitbucket.org/boatmeme/meanwhile/raw/master/doc/images/Synchronous_Strategy.png)

## What is it NOT?
---

Meanwhile is not:

- a Map/Reduce framework
- a Task Scheduling service
- a distributed computation framework 
- a resilient, persistent Job store
- an Intra-app Messaging solution

Though Meanwhile could potentially be used as the foundation for any of these use cases, it does not satisfy any of this functionality out-of-the-box.

It is important to grasp that Meanwhile is not necessarily intended as a general-purpose parallelization library for dividing the data and computation of discrete tasks into subtasks (as in a Map/Reduce scenario). 

Rather, Meanwhile's focus is on performing multiple, self-contained tasks - often unrelated from a data perspective - in a concurrent fashion.

If your requirements include transforming data across tasks in a pipeline, or composing data structures from the results of multiple tasks, your needs may be better served by other libraries.
 

## Task Composition Patterns

----

There are four fundamental patterns for composing tasks with Meanwhile. Most conceivable use cases can be satisfied by some combination of these patterns with either an Asynchronous or Synchronous execution strategy.

### Ad Hoc

An Ad Hoc pattern is useful in scenarios where no one task is dependent on the success or failure of another.

Ad Hoc is often employed in an Asynchronous execution scenario.

A common use case would be as a service for queueing work from a Client application that manages the execution of many instances of the same or similar tasks, particularly where task execution may be triggered by a human user one-at-time or in batch.

![AdHocPattern](https://bitbucket.org/boatmeme/meanwhile/raw/master/doc/images/Pattern_AdHoc.png)

### Chained

![ChainedPattern](https://bitbucket.org/boatmeme/meanwhile/raw/master/doc/images/Pattern_Chained.png)

### Chained (Degenerate)

![DegenerateChainedPattern](https://bitbucket.org/boatmeme/meanwhile/raw/master/doc/images/Pattern_DeChain.png)

### Staged

![StagedPattern](https://bitbucket.org/boatmeme/meanwhile/raw/master/doc/images/Pattern_Staged.png)

# Change Log

## v0.3.1 - 2012.10.04

* Created a new TaskQueueManager class which acts as a static interface for managing one or more uniquely named TaskQueue instances.

## v0.3.0 - 2012.10.03

* Refactored TaskManager into a non-static TaskQueue, which allows for running more than one task queue per JVM

## v0.2.1 - 2012.03.19

* Added block() to TaskManager to support blocking on Tasks that are already In-Flight (useful for Unit Testing client apps)
* Fixed bug with Cancel by UUID String
* Fixed bug with Stages and Nested Chains - Stage onFailure() was not called if a nested, chained Task failed
* Overloaded BaseChain.chain() to accept a List of Tasks to be chained

## v0.2 - 2012.02.15 

* First release as a stand-alone library

## v0.1 - 2011.08

* Initial release embedded in PRO for Change Package functionality

