package com.level3.meanwhile;

import com.level3.meanwhile.state.TaskStatus;
import com.level3.meanwhile.state.ClaimCheck;
import com.level3.meanwhile.base.BaseChain;
import com.level3.meanwhile.base.BaseTask;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * Tests for the TaskManager - submits, pool resizing, queuing, task cancellation, etc.
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public class TaskQueueManagerTest extends MeanwhileTest {
    private static String QUEUE_NAME = "mytest";
    
    public TaskQueueManagerTest() {
    }
    
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Override
    @After
    public void tearDown() {
        super.tearDown();
        TaskQueueManager.shutdown(QUEUE_NAME);
    }

    /**
     * Test of main method, of class TaskManager.
     */
    @Test
    public void testExecute() throws Exception {
        final boolean passed = true;
        
        MeanwhileTest.LifecycleTask task = TaskQueueManager.blockingSubmit(QUEUE_NAME,
                new MeanwhileTest.LifecycleTask() {}
        );
        assertNotNull(task);
        assertTrue("Task not completed",task.completed);
        assertTrue("Task not executed",task.executed);
        assertTrue("Task not submitted",task.submitted);
        assertTrue("Task not succeeded",task.succeeded);
        assertFalse("Task failed",task.failed);
    }
    
    @Test
    public void testGenericExecute() throws Exception {
        
        List<BaseTask> taskList = new ArrayList<BaseTask>();
            taskList.add(new MeanwhileTest.SuccessTask());
            taskList.add(new MeanwhileTest.FailTask());
        
        List<BaseTask> returnTasks = TaskQueueManager.blockingSubmit(QUEUE_NAME,taskList);
        
        assertTrue("ReturnTasks is size 0",returnTasks.size()>0);
        assertTrue("ReturnTasks is not size 2",returnTasks.size()==2);
        assertEquals("Return Task has failed",TaskStatus.SUCCESS,returnTasks.get(0).getStatus());
        assertEquals("Return Task has not",TaskStatus.FAILED,returnTasks.get(1).getStatus());
    }
    
    @Test
    public void testGrowPool() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        BaseTask task1 = new MeanwhileTest.TimerTask(500L);
        BaseTask task2 = new MeanwhileTest.TimerTask(500L);
        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        
        TaskQueueManager.submit(QUEUE_NAME,taskList);
        stopWatch.block(100);
        assertEquals("TaskManger active size = 1",1,TaskQueueManager.getActiveCount(QUEUE_NAME));
        // Block long enough for first task to complete and second task to begin
        stopWatch.block(600);
        stopWatch.block(100);
        assertEquals("TaskManger active size = 1",1,TaskQueueManager.getActiveCount(QUEUE_NAME));
        assertEquals("Task 1 SUCCESS with 1 thread",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 WORKING with 1 thread",TaskStatus.WORKING,task2.getStatus());
        assertEquals("TaskManger pool size = 1",1,TaskQueueManager.getMaximumPoolSize(QUEUE_NAME));
        
        //Wait until the second task completes
        stopWatch.block(400);
        
        //Set thread count to 2
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,2);
        
        task1 = new MeanwhileTest.TimerTask(500L);
        task2 = new MeanwhileTest.TimerTask(500L);
        BaseTask task3 = new MeanwhileTest.TimerTask(500L);
        
        taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task3);
        
        TaskQueueManager.submit(QUEUE_NAME,taskList);
        stopWatch.block(100);
        assertEquals("TaskManger active size = 2",2,TaskQueueManager.getActiveCount(QUEUE_NAME));
        // Block long enough for first two tasks to complete and third task to begin
        stopWatch.block(800);
        assertEquals("TaskManger active size = 1",1,TaskQueueManager.getActiveCount(QUEUE_NAME));
        assertEquals("Task 1 SUCCESS with 2 threads",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 SUCCESS with 2 threads",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 WORKING with 2 threads",TaskStatus.WORKING,task3.getStatus());
        assertEquals("TaskManger pool size = 2",2,TaskQueueManager.getMaximumPoolSize(QUEUE_NAME));
    }    
    
    @Test
    public void testCancelTask() throws Exception {
        
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);

        
        List<BaseChain> taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        List<ClaimCheck> claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        
        boolean canceled = TaskQueueManager.cancel(QUEUE_NAME,claims.get(1));
        assertTrue("task2 report canceled true - Claim Check",canceled);
        assertEquals(TaskStatus.CANCELED,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        stopWatch.block(500);
        
        //Cancel by UUID
        task1 = new MeanwhileTest.TimerTask(500L);
        task2 = new MeanwhileTest.TimerTask(500L);
        task3 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);
        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        
        canceled = TaskQueueManager.cancel(QUEUE_NAME,UUID.fromString(claims.get(1).toString()));
        assertTrue("task2 report canceled true - UUID",canceled);
        assertEquals(TaskStatus.CANCELED,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        stopWatch.block(500);
        
        //Cancel by UUID String
        task1 = new MeanwhileTest.TimerTask(500L);
        task2 = new MeanwhileTest.TimerTask(500L);
        task3 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);
        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        canceled = TaskQueueManager.cancel(QUEUE_NAME,claims.get(1).toString());
        assertTrue("task2 report canceled true - UUID String",canceled);
        assertEquals(TaskStatus.CANCELED,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        stopWatch.block(500);
        
        //Cancel by and do not queue
        task1 = new MeanwhileTest.TimerTask(500L);
        task2 = new MeanwhileTest.TimerTask(500L);
        task3 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);

        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        
        stopWatch.block(700);
        canceled = TaskQueueManager.cancel(QUEUE_NAME,claims.get(1));
        assertTrue("task3 report canceled true",canceled);
        assertEquals(TaskStatus.WORKING,task2.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task3.getStatus());
        stopWatch.block(1000);
        assertEquals(TaskStatus.SUCCESS,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        //Does not cancel in-flight tasks
        task1 = new MeanwhileTest.TimerTask(500L);
        task2 = new MeanwhileTest.TimerTask(500L);
        
        assertEquals(0,TaskQueueManager.getQueueSize(QUEUE_NAME));
        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        
        stopWatch.block(200);
        canceled = TaskQueueManager.cancel(QUEUE_NAME,claims.get(0));
        assertEquals(TaskStatus.WORKING,task1.getStatus());
        assertEquals(TaskStatus.QUEUED,task2.getStatus());
        
        assertFalse("task1 report canceled false",canceled);
        assertEquals(TaskStatus.WORKING,task1.getStatus());
        assertEquals(TaskStatus.QUEUED,task2.getStatus());
        stopWatch.block(1000);
        assertEquals(TaskStatus.SUCCESS,task1.getStatus());
        assertEquals(TaskStatus.SUCCESS,task2.getStatus());
    }
    
    @Test
    public void testCancelStage() throws Exception {
        
         TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel Stage by Task
        BaseChain task0 = new MeanwhileTest.TimerTask(500L);
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);

        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        stage2.stage(task4);
        stage1.chain(stage2);
        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task0);
        taskList.add(stage1);
        List<ClaimCheck> claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(600); 
        boolean canceled = TaskQueueManager.cancel(QUEUE_NAME,claims.get(1));
        assertTrue("Task 2 canceled = true within Stage",canceled);
        assertEquals("Task 2 status CANCELED within Stage",TaskStatus.DEQUEUED,task2.getStatus());
        assertEquals("Task 3 status CANCELED within Stage",TaskStatus.DEQUEUED,task3.getStatus());
        // Wait for all tasks and stages to complete
        stopWatch.block(2000);
        assertEquals("Task 2 status CANCELED within Stage - post all finish",TaskStatus.CANCELED,task2.getStatus());
        assertEquals("Task 3 status CANCELED within Stage",TaskStatus.CANCELED,task3.getStatus());
        assertEquals("Task 4 status SUCCCESS within Stage",TaskStatus.CANCELED,task4.getStatus());
        assertEquals("Task 0 status SUCCESS outside of stage",TaskStatus.SUCCESS,task0.getStatus());
        assertEquals("Task 1 status SUCCESS within Stage",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Stage 1 status SUCCESS within Stage",TaskStatus.SUCCESS,stage1.getStatus());
        assertEquals("Stage 2 status SUCCESS within Stage",TaskStatus.CANCELED,stage2.getStatus());
    }
    
    @Test
    public void testGetQueueSize() throws Exception {
         TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        TaskQueueManager.submit(QUEUE_NAME,taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(200);
        assertEquals(2,TaskQueueManager.getQueueSize(QUEUE_NAME));
        
    }
    
    @Test
    public void testGetStageQueueSize() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        TaskQueueManager.setMaximumStagePoolThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        BaseChain task5 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);
        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        Stage stage3 = new Stage();
        stage3.stage(task5);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(stage1);
        taskList.add(stage2);
        taskList.add(task4);
        taskList.add(stage3);
        TaskQueueManager.submit(QUEUE_NAME,taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(200);
        assertEquals(2,TaskQueueManager.getStageQueueSize(QUEUE_NAME));
    }
    
    @Test
    public void testGetIsQueued() throws Exception {
         TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        List<ClaimCheck> claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        assertFalse("Task 1 reported as not queued",TaskQueueManager.isQueued(QUEUE_NAME,claims.get(0)));
        assertTrue("Task 2 reported as queued",TaskQueueManager.isQueued(QUEUE_NAME,claims.get(1)));
        assertTrue("Task 4 reported as queued",TaskQueueManager.isQueued(QUEUE_NAME,claims.get(2)));
        
    }
    
    @Test
    public void testGetIsStageQueued() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        TaskQueueManager.setMaximumStagePoolThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        BaseChain task5 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);
        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        Stage stage3 = new Stage();
        stage3.stage(task5);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(stage1);
        taskList.add(stage2);
        taskList.add(task4);
        taskList.add(stage3);
        List<ClaimCheck> claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(600);
        assertFalse("Stage 1 reported as not queued",TaskQueueManager.isQueued(QUEUE_NAME,claims.get(0)));
        assertTrue("Stage 2 reported as queued",TaskQueueManager.isQueued(QUEUE_NAME,claims.get(1)));
        assertTrue("Stage 3 reported as queued",TaskQueueManager.isQueued(QUEUE_NAME,claims.get(3)));
    }
    
    @Test
    public void testGetQueuePosition() throws Exception {
         TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        List<ClaimCheck> claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        assertEquals("Task 1 reported in position -1",-1,TaskQueueManager.getQueuePosition(QUEUE_NAME,claims.get(0)));
        assertEquals("Task 2 reported in position 1",1,TaskQueueManager.getQueuePosition(QUEUE_NAME,claims.get(1)));
        assertEquals("Task 4 reported in position 2",2,TaskQueueManager.getQueuePosition(QUEUE_NAME,claims.get(2)));
        
    }
    
    @Test
    public void testGetStageQueuePosition() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        TaskQueueManager.setMaximumStagePoolThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(2000);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        BaseChain task5 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);
        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        Stage stage3 = new Stage();
        stage3.stage(task5);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(stage1);
        taskList.add(stage2);
        taskList.add(task4);
        taskList.add(stage3);
        List<ClaimCheck> claims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(600);
        assertEquals("Stage 1 reported in position -1",-1,TaskQueueManager.getQueuePosition(QUEUE_NAME,claims.get(0)));
        assertEquals("Stage 2 reported in position 1",1,TaskQueueManager.getQueuePosition(QUEUE_NAME,claims.get(1)));
        assertEquals("Stage 3 reported in position 2",2,TaskQueueManager.getQueuePosition(QUEUE_NAME,claims.get(3)));
    }
    
    @Test
    public void testShowdownPool() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        assertTrue(TaskQueueManager.isPoolStarted(QUEUE_NAME));
        TaskQueueManager.shutdown();
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        assertFalse(TaskQueueManager.isPoolStarted(QUEUE_NAME));
        
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        assertTrue(TaskQueueManager.isPoolStarted(QUEUE_NAME));
        //Cancel by Task
        Task task1 = new MeanwhileTest.TimerTask(1000L);
        
        TaskQueueManager.submit(QUEUE_NAME,task1);
        TaskQueueManager.shutdown(QUEUE_NAME,500);
        assertFalse(TaskQueueManager.isPoolStarted(QUEUE_NAME));
        
    }
    
    @Test
    public void testGetQueuedClaims() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(500L);
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);
        BaseChain task4 = new MeanwhileTest.TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        List<ClaimCheck> returnedClaims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        
        List<ClaimCheck> claims = TaskQueueManager.getQueuedTasks(QUEUE_NAME);
        assertEquals(2,claims.size());
        assertTrue(claims.get(0).equals(returnedClaims.get(1)));
        
    }
    
    @Test
    public void testGetActiveClaims() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(1000L);
        BaseChain task2 = new MeanwhileTest.TimerTask(1000L);
        BaseChain task3 = new MeanwhileTest.TimerTask(1000L);
        BaseChain task4 = new MeanwhileTest.TimerTask(1000L);
        BaseChain task5 = new MeanwhileTest.TimerTask(1000L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        taskList.add(task5);
        List<ClaimCheck> returnedClaims = TaskQueueManager.execute(QUEUE_NAME,taskList);
        stopWatch.block(200);
        //System.out.println(returnedClaims);
        List<ClaimCheck> claims = TaskQueueManager.getActiveTasks(QUEUE_NAME);
        //System.out.println(claims);
        assertEquals(3,claims.size());
        assertTrue(claims.contains(returnedClaims.get(0)));
        assertFalse(claims.contains(returnedClaims.get(3)));
    }
    
    @Test
    public void testBlockOnClaim() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(200L);
        BaseChain task2 = new MeanwhileTest.TimerTask(200L);
        BaseChain task3 = new MeanwhileTest.TimerTask(200L);
        BaseChain task4 = new MeanwhileTest.TimerTask(200L);
        BaseChain task5 = new MeanwhileTest.TimerTask(200L);
        BaseChain independentTask = new MeanwhileTest.TimerTask(200L);
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = TaskQueueManager.execute(QUEUE_NAME,task1);
        ClaimCheck indClaim = TaskQueueManager.execute(QUEUE_NAME,independentTask);
        
        stopWatch.block(300);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        stopWatch.block(250);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is WORKING",TaskStatus.WORKING,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean indSuccess = TaskQueueManager.block(QUEUE_NAME,indClaim);
        assertFalse("Independent Block was unsuccessful",indSuccess); 
        
        boolean success = TaskQueueManager.block(QUEUE_NAME,claim);
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnUUIDString() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(200L);
        BaseChain task2 = new MeanwhileTest.TimerTask(200L);
        BaseChain task3 = new MeanwhileTest.TimerTask(200L);
        BaseChain task4 = new MeanwhileTest.TimerTask(200L);
        BaseChain task5 = new MeanwhileTest.TimerTask(200L);
        BaseChain independentTask = new MeanwhileTest.TimerTask(200L);
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = TaskQueueManager.execute(QUEUE_NAME,task1);
        ClaimCheck indClaim = TaskQueueManager.execute(QUEUE_NAME,independentTask);
        
        stopWatch.block(300);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        stopWatch.block(250);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is WORKING",TaskStatus.WORKING,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean indSuccess = TaskQueueManager.block(QUEUE_NAME,indClaim.toString());
        assertFalse("Independent Block was unsuccessful",indSuccess); 
        
        boolean success = TaskQueueManager.block(QUEUE_NAME,claim.toString());
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnUUID() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(200L);
        BaseChain task2 = new MeanwhileTest.TimerTask(200L);
        BaseChain task3 = new MeanwhileTest.TimerTask(200L);
        BaseChain task4 = new MeanwhileTest.TimerTask(200L);
        BaseChain task5 = new MeanwhileTest.TimerTask(200L);
        BaseChain independentTask = new MeanwhileTest.TimerTask(200L);
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = TaskQueueManager.execute(QUEUE_NAME,task1);
        ClaimCheck indClaim = TaskQueueManager.execute(QUEUE_NAME,independentTask);
        
        stopWatch.block(300);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        stopWatch.block(250);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is WORKING",TaskStatus.WORKING,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean indSuccess = TaskQueueManager.block(QUEUE_NAME,UUID.fromString(indClaim.toString()));
        assertFalse("Independent Block was unsuccessful",indSuccess); 
        
        boolean success = TaskQueueManager.block(QUEUE_NAME,UUID.fromString(claim.toString()));
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnClaimDuringStage() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        Stage task1 = new Stage();
        BaseChain task2 = new MeanwhileTest.TimerTask(300L);
        BaseChain task3 = new MeanwhileTest.TimerTask(200L);
        BaseChain task4 = new MeanwhileTest.TimerTask(200L);
        BaseChain task5 = new MeanwhileTest.TimerTask(200L);
        BaseChain independentTask = new MeanwhileTest.TimerTask(200L);
        
        task1.stage(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = TaskQueueManager.execute(QUEUE_NAME,task1);
        
        stopWatch.block(100);
        assertEquals("Stage 1 is WORKING",TaskStatus.WORKING,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean success = TaskQueueManager.block(QUEUE_NAME,claim);
        assertEquals("Stage 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnClaimQueued() throws Exception {
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new MeanwhileTest.TimerTask(200L);
        BaseChain task2 = new MeanwhileTest.TimerTask(200L);
        BaseChain task3 = new MeanwhileTest.TimerTask(200L);
        BaseChain task4 = new MeanwhileTest.TimerTask(200L);
        BaseChain task5 = new MeanwhileTest.TimerTask(200L);
        BaseChain independentTask = new MeanwhileTest.TimerTask(200L);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(task1);
        tasks.add(task2);
        
        List<ClaimCheck> claims = TaskQueueManager.execute(QUEUE_NAME,tasks);
        
        stopWatch.block(100);
        assertEquals("Task 1 is WORKING",TaskStatus.WORKING,task1.getStatus());
        assertEquals("Task 2 is QUEUED",TaskStatus.QUEUED,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean success = TaskQueueManager.block(QUEUE_NAME,claims.get(1));
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testMultipleTaskManagers() throws Exception {
        final String OTHER_QUEUE = "otherQueue";
        TaskQueueManager.setMaximumThreadCount(OTHER_QUEUE,1);
        TaskQueueManager.setMaximumThreadCount(QUEUE_NAME,1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task2 = new MeanwhileTest.TimerTask(500L);
        BaseChain task3 = new MeanwhileTest.TimerTask(500L);

        BaseChain task1a = new MeanwhileTest.TimerTask(100L);
        BaseChain task2a = new MeanwhileTest.TimerTask(100L);
        BaseChain task3a = new MeanwhileTest.TimerTask(100L);
        
        task2a.chain(task3a);
        task1a.chain(task2a);
        
        TaskQueueManager.execute(QUEUE_NAME,task2);
        TaskQueueManager.execute(QUEUE_NAME,task3);
        TaskQueueManager.execute(OTHER_QUEUE, task1a);
        stopWatch.block(400);
        
        assertEquals(TaskStatus.SUCCESS,task1a.getStatus());
        assertEquals(TaskStatus.SUCCESS,task2a.getStatus());
        assertEquals(TaskStatus.SUCCESS,task3a.getStatus());
        assertEquals(TaskStatus.WORKING,task2.getStatus());
        assertEquals(TaskStatus.QUEUED,task3.getStatus());
    }
}
