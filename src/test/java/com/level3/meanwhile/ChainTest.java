package com.level3.meanwhile;

import com.level3.meanwhile.state.TaskStatus;
import com.level3.meanwhile.base.BaseTask;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import static org.junit.Assert.*;
import org.junit.*;

/**
 * 
 * Tests for Chain - execution order, status transitions, etc.
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public class ChainTest extends MeanwhileTest {
    
    public ChainTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Override
    @After
    public void tearDown() {
    }
    
    @Test
    public void testChainedExecute() throws Exception {
        
        MeanwhileTest.SuccessTask task1 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.SuccessTask task2 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.FailTask task3 = new MeanwhileTest.FailTask();
        MeanwhileTest.SuccessTask task4 = new MeanwhileTest.SuccessTask();
        BaseTask task5 = new BaseTask() {
            @Override
            public boolean execute() {
                return true;
            }
        };
        
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        List<BaseTask> taskList = new ArrayList<BaseTask>();
        taskList.add(task1);
        
        List<BaseTask> returnTasks = manager.blockingSubmit(taskList);
        
        assertTrue("ReturnTasks is size 0",returnTasks.size()>0);
        assertTrue("ReturnTasks is not size 2",returnTasks.size()==1);
        assertEquals("Task 1 has failed",TaskStatus.SUCCESS,returnTasks.get(0).getStatus());
        assertEquals("Task 1 has failed",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 has failed",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 has not FAILED",TaskStatus.FAILED,task3.getStatus());
        assertEquals("Task 4 is not CANCELED",TaskStatus.CANCELED,task4.getStatus());
        assertEquals("Task 4 is not CANCELED",TaskStatus.CANCELED,task5.getStatus());
        /*
        assertFalse("Task 1 reported ALL SUCCESS",task1.allSuccess());
        assertFalse("Task 2 reported ALL SUCCESS",task2.allSuccess());
        assertFalse("Task 3 reported ALL SUCCESS",task3.allSuccess());
        assertFalse("Task 4 reported ALL SUCCESS",task4.allSuccess());
        */   
    }
    
    @Test
    public void testChainedExecuteList() throws Exception {
        
        MeanwhileTest.SuccessTask task1 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.SuccessTask task2b = new MeanwhileTest.SuccessTask();
        MeanwhileTest.SuccessTask task2 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.FailTask task3 = new MeanwhileTest.FailTask();
        MeanwhileTest.SuccessTask task4 = new MeanwhileTest.SuccessTask();
        BaseTask task5 = new BaseTask() {
            @Override
            public boolean execute() {
                return true;
            }
        };
        
        List<Task> taskChainList = new ArrayList<Task>();
        taskChainList.add(task2);
        taskChainList.add(task2b);
        
        task1.chain(taskChainList);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        List<BaseTask> taskList = new ArrayList<BaseTask>();
        taskList.add(task1);
        
        List<BaseTask> returnTasks = manager.blockingSubmit(taskList);
        
        assertTrue("ReturnTasks is size 0",returnTasks.size()>0);
        assertTrue("ReturnTasks is not size 2",returnTasks.size()==1);
        assertEquals("Task 1 has failed",TaskStatus.SUCCESS,returnTasks.get(0).getStatus());
        assertEquals("Task 1 has failed",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 has failed",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 has not FAILED",TaskStatus.FAILED,task3.getStatus());
        assertEquals("Task 4 is not CANCELED",TaskStatus.CANCELED,task4.getStatus());
        assertEquals("Task 4 is not CANCELED",TaskStatus.CANCELED,task5.getStatus());
    }
    
    @Test
    public void testChainedExecuteTiming() throws Exception {
        
        MeanwhileTest.TimerTask task1 = new MeanwhileTest.TimerTask(500L); 
        MeanwhileTest.SuccessTask task2 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.SuccessTask task3 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.TimerTask task4 = new MeanwhileTest.TimerTask(500L);
        
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        
        stopWatch.start();
        manager.blockingSubmit(taskList);
        long newTime = stopWatch.currentElapsed();
        
        assertTrue("Blocking submit took less than 1 seconds: " + newTime + "ms",newTime >= 1000);
    }
    
    @Test
    public void testRetryChained() throws Exception {
        
        final MeanwhileTest.RetryTask task1 = new MeanwhileTest.RetryTask(200);
        
        final MeanwhileTest.RetryTask task2 = new MeanwhileTest.RetryTask(200);
        
        List<Task> list = new ArrayList<Task>();
        list.add(task2);
        
        task1.setChainedTasks(list);
        
        MeanwhileTest.RetryTask task = manager.blockingSubmit(task1);
        
        assertNotNull(task);
        assertTrue(TaskStatus.SUCCESS.equals(task1.getStatus()));
        assertTrue(TaskStatus.SUCCESS.equals(task2.getStatus()));
    }
    
    @Test
    public void testChainedNotBlocking() throws Exception {
        
        final MeanwhileTest.TimerTask task1 = new MeanwhileTest.TimerTask(200L);
        
        final MeanwhileTest.TimerTask task2 = new MeanwhileTest.TimerTask(200L);
        
        List<Task> list = new ArrayList<Task>();
        list.add(task2);
        
        task1.setChainedTasks(list);
        
        manager.execute(task1);
        
        stopWatch.block(100L);
        assertTrue(TaskStatus.WORKING.equals(task1.getStatus()));
        assertTrue(TaskStatus.DEQUEUED.equals(task2.getStatus()));
        
        stopWatch.block(150L);
        assertTrue(TaskStatus.SUCCESS.equals(task1.getStatus()));
        assertTrue(TaskStatus.WORKING.equals(task2.getStatus()));
    }
    
    @Test
    public void testClearChained() throws Exception {
        
        final MeanwhileTest.RetryTask task1 = new MeanwhileTest.RetryTask(200);
        
        final MeanwhileTest.RetryTask task2 = new MeanwhileTest.RetryTask(200);
        
        task1.chain(task2);
        
        
        assertNotNull(task1.getChainedTasks());
        assertTrue(task1.getChainedTasks().size()==1);
        task1.clearChainedTasks();
        assertNotNull(task1.getChainedTasks());
        assertTrue(task1.getChainedTasks().isEmpty());
    }
    
    @Test
    public void testInsertLink() throws Exception {
        
        MeanwhileTest.TimerTask task1 = new MeanwhileTest.TimerTask(500L); 
        MeanwhileTest.SuccessTask task2 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.SuccessTask task3 = new MeanwhileTest.SuccessTask();
        MeanwhileTest.TimerTask task4 = new MeanwhileTest.TimerTask(100L);
        MeanwhileTest.TimerTask insertTask = new MeanwhileTest.TimerTask(500L);
        
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        
        manager.execute(taskList);
        stopWatch.block(200);
        
        assertEquals(TaskStatus.WORKING,task1.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task2.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task4.getStatus());
        
        task1.insertLink(insertTask);
        
        stopWatch.block(400);
        
        assertEquals(TaskStatus.SUCCESS,task1.getStatus());
        assertEquals(TaskStatus.WORKING,insertTask.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task2.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task4.getStatus());
        
        stopWatch.block(600);
        
        assertEquals(TaskStatus.SUCCESS,task1.getStatus());
        assertEquals(TaskStatus.SUCCESS,task2.getStatus());
        assertEquals(TaskStatus.SUCCESS,task2.getStatus());
        assertEquals(TaskStatus.SUCCESS,task3.getStatus());
        assertEquals(TaskStatus.SUCCESS,task4.getStatus());
    }
    
}
