/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.level3.meanwhile;

import com.level3.meanwhile.state.TaskStatus;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author boatmeme
 */
public class TaskStatusTest extends MeanwhileTest{
    
    public TaskStatusTest() {
    }

    /**
     * Test of values method, of class TaskStatus.
     */
    @Test
    public void testValues() {
        TaskStatus[] eResult = {TaskStatus.CANCELED,
            TaskStatus.DEQUEUED,
            TaskStatus.FAILED,
            TaskStatus.QUEUED,
            TaskStatus.RETRY,
            TaskStatus.SUCCESS,
            TaskStatus.WORKING};
        List<TaskStatus> expResult = Arrays.asList(eResult);
        List<TaskStatus> result = Arrays.asList(TaskStatus.values());
        assertTrue("expectedResult containsAll result",result.containsAll(expResult));
        assertTrue("result containsAll expectedResult",expResult.containsAll(result));
    }

    /**
     * Test of valueOf method, of class TaskStatus.
     */
    @Test
    public void testValueOf() {
        String name = "CANCELED";
        TaskStatus expResult = TaskStatus.CANCELED;
        TaskStatus result = TaskStatus.valueOf(name);
        assertEquals(expResult, result);
    }
}
