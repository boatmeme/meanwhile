package com.level3.meanwhile;

import com.level3.meanwhile.state.TaskStatus;
import com.level3.meanwhile.state.ClaimCheck;
import com.level3.meanwhile.base.BaseChain;
import com.level3.meanwhile.base.BaseTask;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * Tests for the manager - submits, pool resizing, queuing, task cancellation, etc.
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public class TaskQueueTest extends MeanwhileTest {
    
    public TaskQueueTest() {
    }
    
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Override
    @After
    public void tearDown() {
        super.tearDown();
    }

    /**
     * Test of main method, of class manager.
     */
    @Test
    public void testExecute() throws Exception {
        final boolean passed = true;
        
        LifecycleTask task = manager.blockingSubmit(
                new LifecycleTask() {}
        );
        assertNotNull(task);
        assertTrue("Task not completed",task.completed);
        assertTrue("Task not executed",task.executed);
        assertTrue("Task not submitted",task.submitted);
        assertTrue("Task not succeeded",task.succeeded);
        assertFalse("Task failed",task.failed);
    }
    
    @Test
    public void testGenericExecute() throws Exception {
        
        List<BaseTask> taskList = new ArrayList<BaseTask>();
            taskList.add(new SuccessTask());
            taskList.add(new FailTask());
        
        List<BaseTask> returnTasks = manager.blockingSubmit(taskList);
        
        assertTrue("ReturnTasks is size 0",returnTasks.size()>0);
        assertTrue("ReturnTasks is not size 2",returnTasks.size()==2);
        assertEquals("Return Task has failed",TaskStatus.SUCCESS,returnTasks.get(0).getStatus());
        assertEquals("Return Task has not",TaskStatus.FAILED,returnTasks.get(1).getStatus());
    }
    
    @Test
    public void testGrowPool() throws Exception {
        manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        BaseTask task1 = new TimerTask(500L);
        BaseTask task2 = new TimerTask(500L);
        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        
        manager.submit(taskList);
        stopWatch.block(100);
        assertEquals("TaskManger active size = 1",1,manager.getActiveCount());
        // Block long enough for first task to complete and second task to begin
        stopWatch.block(600);
        stopWatch.block(100);
        assertEquals("TaskManger active size = 1",1,manager.getActiveCount());
        assertEquals("Task 1 SUCCESS with 1 thread",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 WORKING with 1 thread",TaskStatus.WORKING,task2.getStatus());
        assertEquals("TaskManger pool size = 1",1,manager.getMaximumPoolSize());
        
        //Wait until the second task completes
        stopWatch.block(400);
        
        //Set thread count to 2
        manager.setMaximumThreadCount(2);
        
        task1 = new TimerTask(500L);
        task2 = new TimerTask(500L);
        BaseTask task3 = new TimerTask(500L);
        
        taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task3);
        
        manager.submit(taskList);
        stopWatch.block(100);
        assertEquals("TaskManger active size = 2",2,manager.getActiveCount());
        // Block long enough for first two tasks to complete and third task to begin
        stopWatch.block(800);
        assertEquals("TaskManger active size = 1",1,manager.getActiveCount());
        assertEquals("Task 1 SUCCESS with 2 threads",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 SUCCESS with 2 threads",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 WORKING with 2 threads",TaskStatus.WORKING,task3.getStatus());
        assertEquals("TaskManger pool size = 2",2,manager.getMaximumPoolSize());
    }    
    
    @Test
    public void testCancelTask() throws Exception {
        
        manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        task2.chain(task3);

        
        List<BaseChain> taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        List<ClaimCheck> claims = manager.execute(taskList);
        
        boolean canceled = manager.cancel(claims.get(1));
        assertTrue("task2 report canceled true - Claim Check",canceled);
        assertEquals(TaskStatus.CANCELED,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        stopWatch.block(500);
        
        //Cancel by UUID
        task1 = new TimerTask(500L);
        task2 = new TimerTask(500L);
        task3 = new TimerTask(500L);
        task2.chain(task3);
        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = manager.execute(taskList);
        
        canceled = manager.cancel(UUID.fromString(claims.get(1).toString()));
        assertTrue("task2 report canceled true - UUID",canceled);
        assertEquals(TaskStatus.CANCELED,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        stopWatch.block(500);
        
        //Cancel by UUID String
        task1 = new TimerTask(500L);
        task2 = new TimerTask(500L);
        task3 = new TimerTask(500L);
        task2.chain(task3);
        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = manager.execute(taskList);
        canceled = manager.cancel(claims.get(1).toString());
        assertTrue("task2 report canceled true - UUID String",canceled);
        assertEquals(TaskStatus.CANCELED,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        stopWatch.block(500);
        
        //Cancel by and do not queue
        task1 = new TimerTask(500L);
        task2 = new TimerTask(500L);
        task3 = new TimerTask(500L);
        task2.chain(task3);

        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = manager.execute(taskList);
        
        stopWatch.block(700);
        canceled = manager.cancel(claims.get(1));
        assertTrue("task3 report canceled true",canceled);
        assertEquals(TaskStatus.WORKING,task2.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task3.getStatus());
        stopWatch.block(1000);
        assertEquals(TaskStatus.SUCCESS,task2.getStatus());
        assertEquals(TaskStatus.CANCELED,task3.getStatus());
        
        //Does not cancel in-flight tasks
        task1 = new TimerTask(500L);
        task2 = new TimerTask(500L);
        
        assertEquals(0,manager.getQueueSize());
        
        taskList = new ArrayList<BaseChain>();
        taskList.add(task1);
        taskList.add(task2);
        claims = manager.execute(taskList);
        
        stopWatch.block(200);
        canceled = manager.cancel(claims.get(0));
        assertEquals(TaskStatus.WORKING,task1.getStatus());
        assertEquals(TaskStatus.QUEUED,task2.getStatus());
        
        assertFalse("task1 report canceled false",canceled);
        assertEquals(TaskStatus.WORKING,task1.getStatus());
        assertEquals(TaskStatus.QUEUED,task2.getStatus());
        stopWatch.block(1000);
        assertEquals(TaskStatus.SUCCESS,task1.getStatus());
        assertEquals(TaskStatus.SUCCESS,task2.getStatus());
    }
    
    @Test
    public void testCancelStage() throws Exception {
        
         manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel Stage by Task
        BaseChain task0 = new TimerTask(500L);
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        task2.chain(task3);

        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        stage2.stage(task4);
        stage1.chain(stage2);
        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task0);
        taskList.add(stage1);
        List<ClaimCheck> claims = manager.execute(taskList);
        
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(600); 
        boolean canceled = manager.cancel(claims.get(1));
        assertTrue("Task 2 canceled = true within Stage",canceled);
        assertEquals("Task 2 status CANCELED within Stage",TaskStatus.DEQUEUED,task2.getStatus());
        assertEquals("Task 3 status CANCELED within Stage",TaskStatus.DEQUEUED,task3.getStatus());
        // Wait for all tasks and stages to complete
        stopWatch.block(2000);
        assertEquals("Task 2 status CANCELED within Stage - post all finish",TaskStatus.CANCELED,task2.getStatus());
        assertEquals("Task 3 status CANCELED within Stage",TaskStatus.CANCELED,task3.getStatus());
        assertEquals("Task 4 status SUCCCESS within Stage",TaskStatus.CANCELED,task4.getStatus());
        assertEquals("Task 0 status SUCCESS outside of stage",TaskStatus.SUCCESS,task0.getStatus());
        assertEquals("Task 1 status SUCCESS within Stage",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Stage 1 status SUCCESS within Stage",TaskStatus.SUCCESS,stage1.getStatus());
        assertEquals("Stage 2 status SUCCESS within Stage",TaskStatus.CANCELED,stage2.getStatus());
    }
    
    @Test
    public void testGetQueueSize() throws Exception {
         manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        manager.submit(taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(200);
        assertEquals(2,manager.getQueueSize());
        
    }
    
    @Test
    public void testGetStageQueueSize() throws Exception {
        manager.setMaximumThreadCount(1);
        manager.setMaximumStagePoolThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        BaseChain task5 = new TimerTask(500L);
        task2.chain(task3);
        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        Stage stage3 = new Stage();
        stage3.stage(task5);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(stage1);
        taskList.add(stage2);
        taskList.add(task4);
        taskList.add(stage3);
        manager.submit(taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(200);
        assertEquals(2,manager.getStageQueueSize());
    }
    
    @Test
    public void testGetIsQueued() throws Exception {
         manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        List<ClaimCheck> claims = manager.execute(taskList);
        assertFalse("Task 1 reported as not queued",manager.isQueued(claims.get(0)));
        assertTrue("Task 2 reported as queued",manager.isQueued(claims.get(1)));
        assertTrue("Task 4 reported as queued",manager.isQueued(claims.get(2)));
        
    }
    
    @Test
    public void testGetIsStageQueued() throws Exception {
        manager.setMaximumThreadCount(1);
        manager.setMaximumStagePoolThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        BaseChain task5 = new TimerTask(500L);
        task2.chain(task3);
        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        Stage stage3 = new Stage();
        stage3.stage(task5);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(stage1);
        taskList.add(stage2);
        taskList.add(task4);
        taskList.add(stage3);
        List<ClaimCheck> claims = manager.execute(taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(600);
        assertFalse("Stage 1 reported as not queued",manager.isQueued(claims.get(0)));
        assertTrue("Stage 2 reported as queued",manager.isQueued(claims.get(1)));
        assertTrue("Stage 3 reported as queued",manager.isQueued(claims.get(3)));
    }
    
    @Test
    public void testGetQueuePosition() throws Exception {
         manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        List<ClaimCheck> claims = manager.execute(taskList);
        assertEquals("Task 1 reported in position -1",-1,manager.getQueuePosition(claims.get(0)));
        assertEquals("Task 2 reported in position 1",1,manager.getQueuePosition(claims.get(1)));
        assertEquals("Task 4 reported in position 2",2,manager.getQueuePosition(claims.get(2)));
        
    }
    
    @Test
    public void testGetStageQueuePosition() throws Exception {
        manager.setMaximumThreadCount(1);
        manager.setMaximumStagePoolThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(2000);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        BaseChain task5 = new TimerTask(500L);
        task2.chain(task3);
        Stage stage1 = new Stage();
        stage1.stage(task1);
        Stage stage2 = new Stage();
        stage2.stage(task2);
        Stage stage3 = new Stage();
        stage3.stage(task5);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(stage1);
        taskList.add(stage2);
        taskList.add(task4);
        taskList.add(stage3);
        List<ClaimCheck> claims = manager.execute(taskList);
        // Give it time to proccess MeanwhileThreadPoolExecutor.beforeExecute()
        stopWatch.block(600);
        assertEquals("Stage 1 reported in position -1",-1,manager.getQueuePosition(claims.get(0)));
        assertEquals("Stage 2 reported in position 1",1,manager.getQueuePosition(claims.get(1)));
        assertEquals("Stage 3 reported in position 2",2,manager.getQueuePosition(claims.get(3)));
    }
    
    @Test
    public void testShowdownPool() throws Exception {
        manager.setMaximumThreadCount(1);
        assertTrue(manager.isPoolStarted());
        manager.shutdown();
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        assertFalse(manager.isPoolStarted());
        
        manager.setMaximumThreadCount(1);
        assertTrue(manager.isPoolStarted());
        //Cancel by Task
        Task task1 = new TimerTask(1000L);
        
        manager.submit(task1);
        manager.shutdown(500);
        assertFalse(manager.isPoolStarted());
        
    }
    
    @Test
    public void testGetQueuedClaims() throws Exception {
        manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(500L);
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);
        BaseChain task4 = new TimerTask(500L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        List<ClaimCheck> returnedClaims = manager.execute(taskList);
        
        List<ClaimCheck> claims = manager.getQueuedTasks();
        assertEquals(2,claims.size());
        assertTrue(claims.get(0).equals(returnedClaims.get(1)));
        
    }
    
    @Test
    public void testGetActiveClaims() throws Exception {
        manager.setMaximumThreadCount(3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(1000L);
        BaseChain task2 = new TimerTask(1000L);
        BaseChain task3 = new TimerTask(1000L);
        BaseChain task4 = new TimerTask(1000L);
        BaseChain task5 = new TimerTask(1000L);
        task2.chain(task3);

        
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task4);
        taskList.add(task5);
        List<ClaimCheck> returnedClaims = manager.execute(taskList);
        stopWatch.block(200);
        //System.out.println(returnedClaims);
        List<ClaimCheck> claims = manager.getActiveTasks();
        //System.out.println(claims);
        assertEquals(3,claims.size());
        assertTrue(claims.contains(returnedClaims.get(0)));
        assertFalse(claims.contains(returnedClaims.get(3)));
    }
    
    @Test
    public void testBlockOnClaim() throws Exception {
        manager.setMaximumThreadCount(3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(200L);
        BaseChain task2 = new TimerTask(200L);
        BaseChain task3 = new TimerTask(200L);
        BaseChain task4 = new TimerTask(200L);
        BaseChain task5 = new TimerTask(200L);
        BaseChain independentTask = new TimerTask(200L);
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = manager.execute(task1);
        ClaimCheck indClaim = manager.execute(independentTask);
        
        stopWatch.block(300);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        stopWatch.block(250);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is WORKING",TaskStatus.WORKING,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean indSuccess = manager.block(indClaim);
        assertFalse("Independent Block was unsuccessful",indSuccess); 
        
        boolean success = manager.block(claim);
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnUUIDString() throws Exception {
        manager.setMaximumThreadCount(3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(200L);
        BaseChain task2 = new TimerTask(200L);
        BaseChain task3 = new TimerTask(200L);
        BaseChain task4 = new TimerTask(200L);
        BaseChain task5 = new TimerTask(200L);
        BaseChain independentTask = new TimerTask(200L);
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = manager.execute(task1);
        ClaimCheck indClaim = manager.execute(independentTask);
        
        stopWatch.block(300);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        stopWatch.block(250);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is WORKING",TaskStatus.WORKING,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean indSuccess = manager.block(indClaim.toString());
        assertFalse("Independent Block was unsuccessful",indSuccess); 
        
        boolean success = manager.block(claim.toString());
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnUUID() throws Exception {
        manager.setMaximumThreadCount(3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(200L);
        BaseChain task2 = new TimerTask(200L);
        BaseChain task3 = new TimerTask(200L);
        BaseChain task4 = new TimerTask(200L);
        BaseChain task5 = new TimerTask(200L);
        BaseChain independentTask = new TimerTask(200L);
        task1.chain(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = manager.execute(task1);
        ClaimCheck indClaim = manager.execute(independentTask);
        
        stopWatch.block(300);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        stopWatch.block(250);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is WORKING",TaskStatus.WORKING,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean indSuccess = manager.block(UUID.fromString(indClaim.toString()));
        assertFalse("Independent Block was unsuccessful",indSuccess); 
        
        boolean success = manager.block(UUID.fromString(claim.toString()));
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnClaimDuringStage() throws Exception {
        manager.setMaximumThreadCount(3);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        Stage task1 = new Stage();
        BaseChain task2 = new TimerTask(300L);
        BaseChain task3 = new TimerTask(200L);
        BaseChain task4 = new TimerTask(200L);
        BaseChain task5 = new TimerTask(200L);
        BaseChain independentTask = new TimerTask(200L);
        
        task1.stage(task2);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        ClaimCheck claim = manager.execute(task1);
        
        stopWatch.block(100);
        assertEquals("Stage 1 is WORKING",TaskStatus.WORKING,task1.getStatus());
        assertEquals("Task 2 is WORKING",TaskStatus.WORKING,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean success = manager.block(claim);
        assertEquals("Stage 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testBlockOnClaimQueued() throws Exception {
        manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task1 = new TimerTask(200L);
        BaseChain task2 = new TimerTask(200L);
        BaseChain task3 = new TimerTask(200L);
        BaseChain task4 = new TimerTask(200L);
        BaseChain task5 = new TimerTask(200L);
        BaseChain independentTask = new TimerTask(200L);
        task2.chain(task3);
        task3.chain(task4);
        task4.chain(task5);
        
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(task1);
        tasks.add(task2);
        
        List<ClaimCheck> claims = manager.execute(tasks);
        
        stopWatch.block(100);
        assertEquals("Task 1 is WORKING",TaskStatus.WORKING,task1.getStatus());
        assertEquals("Task 2 is QUEUED",TaskStatus.QUEUED,task2.getStatus());
        assertEquals("Task 3 is DEQUEUED",TaskStatus.DEQUEUED,task3.getStatus());
        assertEquals("Task 4 is DEQUEUED",TaskStatus.DEQUEUED,task4.getStatus());
        assertEquals("Task 5 is DEQUEUED",TaskStatus.DEQUEUED,task5.getStatus());
        
        boolean success = manager.block(claims.get(1));
        assertTrue("Block was successful",success);
        assertEquals("Task 1 is SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 is SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 3 is SUCCESS",TaskStatus.SUCCESS,task3.getStatus());
        assertEquals("Task 4 is SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 5 is SUCCESS",TaskStatus.SUCCESS,task5.getStatus());
    }
    
    @Test
    public void testMultipleManagers() throws Exception {
        TaskQueue otherManager = new TaskQueue(1,1);
        manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        //Cancel by Task
        BaseChain task2 = new TimerTask(500L);
        BaseChain task3 = new TimerTask(500L);

        BaseChain task1a = new TimerTask(100L);
        BaseChain task2a = new TimerTask(100L);
        BaseChain task3a = new TimerTask(100L);
        
        task2a.chain(task3a);
        task1a.chain(task2a);
        
        manager.execute(task2);
        manager.execute(task3);
        otherManager.execute(task1a);
        stopWatch.block(400);
        
        assertEquals(TaskStatus.SUCCESS,task1a.getStatus());
        assertEquals(TaskStatus.SUCCESS,task2a.getStatus());
        assertEquals(TaskStatus.SUCCESS,task3a.getStatus());
        assertEquals(TaskStatus.WORKING,task2.getStatus());
        assertEquals(TaskStatus.QUEUED,task3.getStatus());
    }
}
