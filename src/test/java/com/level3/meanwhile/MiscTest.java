package com.level3.meanwhile;

import com.level3.meanwhile.concurrent.MeanwhileRunner;
import com.level3.meanwhile.base.BaseTask;
import com.level3.meanwhile.concurrent.MeanwhileFuture;
import com.level3.meanwhile.state.ClaimCheck;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Miscellaneous unit tests - configuration, odd situations, constructors, equals(), hashCode() etc.
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public class MiscTest extends MeanwhileTest {
    
    @Test
    public void testClaimCheckEquality() throws Exception {
        ClaimCheck check1 = new ClaimCheck(UUID.randomUUID());
        ClaimCheck check2 = new ClaimCheck(check1.toString());
        assertTrue(UUID.fromString(check1.toString()).equals(UUID.fromString(check2.toString())));
        assertTrue(check1.equals(check2));
        assertTrue(check2.equals(check1));
        assertTrue(check1.equals(check2.toString()));
        assertTrue(check2.equals(check1.toString()));
        assertTrue(check1.equals(UUID.fromString(check2.toString())));
        assertTrue(check2.equals(UUID.fromString(check1.toString())));
        assertTrue(check1.hashCode()==check2.hashCode());
    }
    
    @Test
    public void testClaimCheckParencyEquality() throws Exception {
        ClaimCheck check1 = new ClaimCheck(UUID.randomUUID());
        ClaimCheck check2 = new ClaimCheck(UUID.randomUUID(),check1);
        assertTrue(check2.equals(check1));
        assertFalse(check1.equals(check2));
    }
    
    @Test
    public void testClaimCheckFooEquality() throws Exception {
        ClaimCheck check1 = new ClaimCheck(UUID.randomUUID());
        Integer check2 = new Integer(53);
        assertFalse(check1.equals(check2));
        assertFalse(check2.equals(check1));
    }
    
    @Test
    public void coverageHacks() throws Exception {
        MeanwhileRunner runner = new MeanwhileRunner(new BaseTask() {

            @Override
            public boolean execute() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        }, manager);
        MeanwhileFuture future = new MeanwhileFuture(runner);
        assertTrue(future.equals(future));
        assertTrue(future.hashCode()==future.hashCode());
        assertTrue(runner.equals(runner));
        assertTrue(runner.hashCode()==runner.hashCode());
    }
    
}
