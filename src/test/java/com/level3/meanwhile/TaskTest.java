/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.level3.meanwhile;

import com.level3.meanwhile.state.TaskStatus;
import com.level3.meanwhile.base.BaseTask;
import com.level3.meanwhile.state.ClaimCheck;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import static org.junit.Assert.*;
import org.junit.*;

/**
 * 
 * Tests for Task - AdHoc and Chained composition, execution order, status transitions, etc.
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public class TaskTest extends MeanwhileTest {
    
    public TaskTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Override
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAnonymousInnerClassFinalVarAccess() throws Exception {
        
        List<BaseTask> taskList = new ArrayList<BaseTask>();
        final List<String> payload = Collections.synchronizedList(new ArrayList<String>());
        
        for(int i = 0;i<100;i++) {
            taskList.add(new BaseTask() {
                
                public boolean execute() {
                    payload.add("" + System.currentTimeMillis());
                    return true;
                }
                
            });
        }
        
        List<BaseTask> returnTasks = manager.blockingSubmit(taskList);
        
        assertNotNull("ReturnTasks is null",returnTasks);
        assertTrue("ReturnTasks is not size 100",returnTasks.size()==100);
        for(BaseTask task : returnTasks) {
            assertEquals("Return Task has failed",TaskStatus.SUCCESS,task.getStatus());
        }
        assertTrue("payload is not size 100",payload.size()==100);
        
    }
    
     @Test
    public void testFailOnExecute() throws Exception {
        
        LifecycleTask task = manager.blockingSubmit(
                new LifecycleTask() {
                    @Override
                    public boolean execute() {
                        throw new RuntimeException("whoops");
                    }
                }
        );
        assertNotNull(task);
        assertTrue("Task  completed",task.completed);
        assertFalse("Task executed",task.executed);
        assertTrue("Task not submitted",task.submitted);
        assertFalse("Task not succeeded",task.succeeded);
        assertTrue("Task not failed",task.failed);
    }
    
    @Test
    public void testRetry() throws Exception {
        RetryTask task = manager.blockingSubmit(new RetryTask(200));
        assertNotNull(task);
        assertEquals(200,task.tryNum);
        assertEquals(TaskStatus.SUCCESS,task.getStatus());
    }
    
    @Test
    public void testCountLifecycleCalls() throws Exception {
        
        CounterTask task = new CounterTask("Task 1");
        CounterTask task2 = new CounterTask("Task 2");
        CounterTask task3 = new CounterTask("Task 3");
        
        Stage stage = new Stage();
        CounterTask task4 = new CounterTask("S1 Task 4");
        CounterTask task5 = new CounterTask("S1 Task 5");
        CounterTask task6 = new CounterTask("S1 Task 6");
        
        stage.stage(task4);
        stage.stage(task5);
        stage.stage(task6);
        
        task3.chain(stage);
        task2.chain(task3);
        task.chain(task2);
        manager.blockingSubmit(task);
        
        assertEquals("Task 1 submit once",1,task.submitCount);
        assertEquals("Task 1 start once",1,task.startCount);
        assertEquals("Task 1 execute once",1,task.executeCount);
        assertEquals("Task 1 complete once",1,task.completeCount);
        assertEquals("Task 1 submit none",0,task.cancelCount);
        assertEquals("Task 1 failure none",0,task.failureCount);
        assertEquals("Task 1 retry none",0,task.retryCount);
        
        assertEquals("Task 2 submit once",1,task2.submitCount);
        assertEquals("Task 2 start once",1,task2.startCount);
        assertEquals("Task 2 execute once",1,task2.executeCount);
        assertEquals("Task 2 complete once",1,task2.completeCount);
        assertEquals("Task 2 submit none",0,task2.cancelCount);
        assertEquals("Task 2 failure none",0,task2.failureCount);
        assertEquals("Task 2 retry none",0,task2.retryCount);
        
        assertEquals("Task 3 submit once",1,task3.submitCount);
        assertEquals("Task 3 start once",1,task3.startCount);
        assertEquals("Task 3 execute once",1,task3.executeCount);
        assertEquals("Task 3 complete once",1,task3.completeCount);
        assertEquals("Task 3 submit none",0,task3.cancelCount);
        assertEquals("Task 3 failure none",0,task3.failureCount);
        assertEquals("Task 3 retry none",0,task3.retryCount);
        
        assertEquals("Task 4 submit once",1,task4.submitCount);
        assertEquals("Task 4 start once",1,task4.startCount);
        assertEquals("Task 4 execute once",1,task4.executeCount);
        assertEquals("Task 4 complete once",1,task4.completeCount);
        assertEquals("Task 4 submit none",0,task4.cancelCount);
        assertEquals("Task 4 failure none",0,task4.failureCount);
        assertEquals("Task 4 retry none",0,task4.retryCount);
        
        assertEquals("Task 5 submit once",1,task5.submitCount);
        assertEquals("Task 5 start once",1,task5.startCount);
        assertEquals("Task 5 execute once",1,task5.executeCount);
        assertEquals("Task 5 complete once",1,task5.completeCount);
        assertEquals("Task 5 submit none",0,task5.cancelCount);
        assertEquals("Task 5 failure none",0,task5.failureCount);
        assertEquals("Task 5 retry none",0,task5.retryCount);
        
        assertEquals("Task 6 submit once",1,task6.submitCount);
        assertEquals("Task 6 start once",1,task6.startCount);
        assertEquals("Task 6 execute once",1,task6.executeCount);
        assertEquals("Task 6 complete once",1,task6.completeCount);
        assertEquals("Task 6 submit none",0,task6.cancelCount);
        assertEquals("Task 6 failure none",0,task6.failureCount);
        assertEquals("Task 6 retry none",0,task6.retryCount);
       
    }
    
    @Test
    public void testCountLifecycleCallsFailure() throws Exception {
        
        CounterTask task = new CounterTask("Task 1");
        CounterTask task2 = new CounterTask("Task 2");
        CounterTask task3 = new CounterTask("Task 3",false);
        
        CounterStage stage = new CounterStage("My Stage");
        CounterTask task4 = new CounterTask("S1 Task 4");
        CounterTask task5 = new CounterTask("S1 Task 5");
        CounterTask task6 = new CounterTask("S1 Task 6");
        
        CounterStage stage2 = new CounterStage("My Stage 2");
        CounterTask task7 = new CounterTask("S1 Task 7");
        CounterTask task8 = new CounterTask("S1 Task 8");
        CounterTask task9 = new CounterTask("S1 Task 9");
        
        stage2.stage(task7);
        stage2.stage(task8);
        stage2.stage(task9);
        
        stage.chain(stage2);
        stage.stage(task4);
        stage.stage(task5);
        stage.stage(task6);
        
        task3.chain(stage);
        task2.chain(task3);
        task.chain(task2);
        manager.blockingSubmit(task);
        
        assertEquals("Task 1 submit once",1,task.submitCount);
        assertEquals("Task 1 start once",1,task.startCount);
        assertEquals("Task 1 execute once",1,task.executeCount);
        assertEquals("Task 1 complete once",1,task.completeCount);
        assertEquals("Task 1 cancel none",0,task.cancelCount);
        assertEquals("Task 1 failure none",0,task.failureCount);
        assertEquals("Task 1 retry none",0,task.retryCount);
        
        assertEquals("Task 2 submit once",1,task2.submitCount);
        assertEquals("Task 2 start once",1,task2.startCount);
        assertEquals("Task 2 execute once",1,task2.executeCount);
        assertEquals("Task 2 complete once",1,task2.completeCount);
        assertEquals("Task 2 cancel none",0,task2.cancelCount);
        assertEquals("Task 2 failure none",0,task2.failureCount);
        assertEquals("Task 2 retry none",0,task2.retryCount);
        
        assertEquals("Task 3 submit once",1,task3.submitCount);
        assertEquals("Task 3 start once",1,task3.startCount);
        assertEquals("Task 3 execute once",1,task3.executeCount);
        assertEquals("Task 3 complete once",1,task3.completeCount);
        assertEquals("Task 3 cancel none",0,task3.cancelCount);
        assertEquals("Task 3 failure none",1,task3.failureCount);
        assertEquals("Task 3 retry none",0,task3.retryCount);
        
        assertEquals("Task 4 submit once",0,task4.submitCount);
        assertEquals("Task 4 start once",0,task4.startCount);
        assertEquals("Task 4 execute once",0,task4.executeCount);
        assertEquals("Task 4 complete once",0,task4.completeCount);
        assertEquals("Task 4 cancel none",1,task4.cancelCount);
        assertEquals("Task 4 failure none",0,task4.failureCount);
        assertEquals("Task 4 retry none",0,task4.retryCount);
        
        assertEquals("Task 5 submit once",0,task5.submitCount);
        assertEquals("Task 5 start once",0,task5.startCount);
        assertEquals("Task 5 execute once",0,task5.executeCount);
        assertEquals("Task 5 complete once",0,task5.completeCount);
        assertEquals("Task 5 cancel none",1,task5.cancelCount);
        assertEquals("Task 5 failure none",0,task5.failureCount);
        assertEquals("Task 5 retry none",0,task5.retryCount);
        
        assertEquals("Task 6 submit once",0,task6.submitCount);
        assertEquals("Task 6 start once",0,task6.startCount);
        assertEquals("Task 6 execute once",0,task6.executeCount);
        assertEquals("Task 6 complete once",0,task6.completeCount);
        assertEquals("Task 6 cancel none",1,task6.cancelCount);
        assertEquals("Task 6 failure none",0,task6.failureCount);
        assertEquals("Task 6 retry none",0,task6.retryCount);
        
        assertEquals("Stage 1 submit once",0,stage.submitCount);
        assertEquals("Stage 1 start once",0,stage.startCount);
        assertEquals("Stage 1 execute once",0,stage.executeCount);
        assertEquals("Stage 1 complete once",0,stage.completeCount);
        assertEquals("Stage 1 cancel none",1,stage.cancelCount);
        assertEquals("Stage 1 failure none",0,stage.failureCount);
        assertEquals("Stage 1 retry none",0,stage.retryCount);
        
        assertEquals("Task 7 submit once",0,task7.submitCount);
        assertEquals("Task 7 start once",0,task7.startCount);
        assertEquals("Task 7 execute once",0,task7.executeCount);
        assertEquals("Task 7 complete once",0,task7.completeCount);
        assertEquals("Task 7 cancel none",1,task7.cancelCount);
        assertEquals("Task 7 failure none",0,task7.failureCount);
        assertEquals("Task 7 retry none",0,task7.retryCount);
        
        assertEquals("Stage 2 submit once",0,stage2.submitCount);
        assertEquals("Stage 2 start once",0,stage2.startCount);
        assertEquals("Stage 2 execute once",0,stage2.executeCount);
        assertEquals("Stage 2 complete once",0,stage2.completeCount);
        assertEquals("Stage 2 cancel none",1,stage2.cancelCount);
        assertEquals("Stage 2 failure none",0,stage2.failureCount);
        assertEquals("Stage 2 retry none",0,stage2.retryCount);
    }
    
    @Test
    public void testCountLifecycleCallsCancelDuringTask() throws Exception {
        
        CounterTask task = new CounterTask("Task 1");
        CounterTask task2 = new CounterTask("Task 2");
        CounterTask task3 = new CounterTask("Task 3",true,300L);
        
        CounterStage stage = new CounterStage("My Stage");
        CounterTask task4 = new CounterTask("S1 Task 4");
        CounterTask task5 = new CounterTask("S1 Task 5");
        CounterTask task6 = new CounterTask("S1 Task 6");
        
        CounterStage stage2 = new CounterStage("My Stage 2");
        CounterTask task7 = new CounterTask("S1 Task 7");
        CounterTask task8 = new CounterTask("S1 Task 8");
        CounterTask task9 = new CounterTask("S1 Task 9");
        
        stage2.stage(task7);
        stage2.stage(task8);
        stage2.stage(task9);
        
        stage.chain(stage2);
        stage.stage(task4);
        stage.stage(task5);
        stage.stage(task6);
        
        task3.chain(stage);
        task2.chain(task3);
        task.chain(task2);
        ClaimCheck claim = manager.execute(task);
        
        stopWatch.block(250);
        manager.cancel(claim);
        stopWatch.block(250);
        
        assertEquals("Task 1 submit once",1,task.submitCount);
        assertEquals("Task 1 start once",1,task.startCount);
        assertEquals("Task 1 execute once",1,task.executeCount);
        assertEquals("Task 1 complete once",1,task.completeCount);
        assertEquals("Task 1 cancel none",0,task.cancelCount);
        assertEquals("Task 1 failure none",0,task.failureCount);
        assertEquals("Task 1 retry none",0,task.retryCount);
        
        assertEquals("Task 2 submit once",1,task2.submitCount);
        assertEquals("Task 2 start once",1,task2.startCount);
        assertEquals("Task 2 execute once",1,task2.executeCount);
        assertEquals("Task 2 complete once",1,task2.completeCount);
        assertEquals("Task 2 cancel none",0,task2.cancelCount);
        assertEquals("Task 2 failure none",0,task2.failureCount);
        assertEquals("Task 2 retry none",0,task2.retryCount);
        
        assertEquals("Task 3 submit once",1,task3.submitCount);
        assertEquals("Task 3 start once",1,task3.startCount);
        assertEquals("Task 3 execute once",1,task3.executeCount);
        assertEquals("Task 3 complete once",1,task3.completeCount);
        assertEquals("Task 3 cancel none",0,task3.cancelCount);
        assertEquals("Task 3 failure none",0,task3.failureCount);
        assertEquals("Task 3 retry none",0,task3.retryCount);
        
        assertEquals("Task 4 submit once",0,task4.submitCount);
        assertEquals("Task 4 start once",0,task4.startCount);
        assertEquals("Task 4 execute once",0,task4.executeCount);
        assertEquals("Task 4 complete once",0,task4.completeCount);
        assertEquals("Task 4 cancel none",1,task4.cancelCount);
        assertEquals("Task 4 failure none",0,task4.failureCount);
        assertEquals("Task 4 retry none",0,task4.retryCount);
        
        assertEquals("Task 5 submit once",0,task5.submitCount);
        assertEquals("Task 5 start once",0,task5.startCount);
        assertEquals("Task 5 execute once",0,task5.executeCount);
        assertEquals("Task 5 complete once",0,task5.completeCount);
        assertEquals("Task 5 cancel none",1,task5.cancelCount);
        assertEquals("Task 5 failure none",0,task5.failureCount);
        assertEquals("Task 5 retry none",0,task5.retryCount);
        
        assertEquals("Task 6 submit once",0,task6.submitCount);
        assertEquals("Task 6 start once",0,task6.startCount);
        assertEquals("Task 6 execute once",0,task6.executeCount);
        assertEquals("Task 6 complete once",0,task6.completeCount);
        assertEquals("Task 6 cancel none",1,task6.cancelCount);
        assertEquals("Task 6 failure none",0,task6.failureCount);
        assertEquals("Task 6 retry none",0,task6.retryCount);
        
        assertEquals("Stage 1 submit once",0,stage.submitCount);
        assertEquals("Stage 1 start once",0,stage.startCount);
        assertEquals("Stage 1 execute once",0,stage.executeCount);
        assertEquals("Stage 1 complete once",0,stage.completeCount);
        assertEquals("Stage 1 cancel none",1,stage.cancelCount);
        assertEquals("Stage 1 failure none",0,stage.failureCount);
        assertEquals("Stage 1 retry none",0,stage.retryCount);
        
        assertEquals("Task 7 submit once",0,task7.submitCount);
        assertEquals("Task 7 start once",0,task7.startCount);
        assertEquals("Task 7 execute once",0,task7.executeCount);
        assertEquals("Task 7 complete once",0,task7.completeCount);
        assertEquals("Task 7 cancel none",1,task7.cancelCount);
        assertEquals("Task 7 failure none",0,task7.failureCount);
        assertEquals("Task 7 retry none",0,task7.retryCount);
        
        assertEquals("Stage 2 submit once",0,stage2.submitCount);
        assertEquals("Stage 2 start once",0,stage2.startCount);
        assertEquals("Stage 2 execute once",0,stage2.executeCount);
        assertEquals("Stage 2 complete once",0,stage2.completeCount);
        assertEquals("Stage 2 cancel none",1,stage2.cancelCount);
        assertEquals("Stage 2 failure none",0,stage2.failureCount);
        assertEquals("Stage 2 retry none",0,stage2.retryCount);
    }
    
    @Test
    public void testCountLifecycleCallsCancelDuringStage() throws Exception {
        manager.setMaximumThreadCount(1);
        //Allow some time to bring the thread count back down
        stopWatch.block(500);
        
        CounterTask task = new CounterTask("Task 1");
        CounterTask task2 = new CounterTask("Task 2");
        CounterTask task3 = new CounterTask("Task 3");
        
        CounterStage stage = new CounterStage("My Stage");
        CounterTask task4 = new CounterTask("S1 Task 4",true,300L);
        CounterTask task5 = new CounterTask("S1 Task 5");
        CounterTask task6 = new CounterTask("S1 Task 6");
        
        CounterStage stage2 = new CounterStage("My Stage 2");
        CounterTask task7 = new CounterTask("S1 Task 7");
        CounterTask task8 = new CounterTask("S1 Task 8");
        CounterTask task9 = new CounterTask("S1 Task 9");
        
        stage2.stage(task7);
        stage2.stage(task8);
        stage2.stage(task9);
        
        stage.chain(stage2);
        stage.stage(task4);
        stage.stage(task5);
        stage.stage(task6);
        
        task3.chain(stage);
        task2.chain(task3);
        task.chain(task2);
        ClaimCheck claim = manager.execute(task);
        
        stopWatch.block(250);
        manager.cancel(claim);
        stopWatch.block(350);
        
        assertEquals("Task 1 submit once",1,task.submitCount);
        assertEquals("Task 1 start once",1,task.startCount);
        assertEquals("Task 1 execute once",1,task.executeCount);
        assertEquals("Task 1 complete once",1,task.completeCount);
        assertEquals("Task 1 cancel none",0,task.cancelCount);
        assertEquals("Task 1 failure none",0,task.failureCount);
        assertEquals("Task 1 retry none",0,task.retryCount);
        
        assertEquals("Task 2 submit once",1,task2.submitCount);
        assertEquals("Task 2 start once",1,task2.startCount);
        assertEquals("Task 2 execute once",1,task2.executeCount);
        assertEquals("Task 2 complete once",1,task2.completeCount);
        assertEquals("Task 2 cancel none",0,task2.cancelCount);
        assertEquals("Task 2 failure none",0,task2.failureCount);
        assertEquals("Task 2 retry none",0,task2.retryCount);
        
        assertEquals("Task 3 submit once",1,task3.submitCount);
        assertEquals("Task 3 start once",1,task3.startCount);
        assertEquals("Task 3 execute once",1,task3.executeCount);
        assertEquals("Task 3 complete once",1,task3.completeCount);
        assertEquals("Task 3 cancel none",0,task3.cancelCount);
        assertEquals("Task 3 failure none",0,task3.failureCount);
        assertEquals("Task 3 retry none",0,task3.retryCount);
        
        assertEquals("Task 4 submit once",1,task4.submitCount);
        assertEquals("Task 4 start once",1,task4.startCount);
        assertEquals("Task 4 execute once",1,task4.executeCount);
        assertEquals("Task 4 complete once",1,task4.completeCount);
        assertEquals("Task 4 cancel none",0,task4.cancelCount);
        assertEquals("Task 4 failure none",0,task4.failureCount);
        assertEquals("Task 4 retry none",0,task4.retryCount);
        
        assertEquals("Task 5 submit once",1,task5.submitCount);
        assertEquals("Task 5 start once",0,task5.startCount);
        assertEquals("Task 5 execute once",0,task5.executeCount);
        assertEquals("Task 5 complete once",0,task5.completeCount);
        assertEquals("Task 5 cancel none",1,task5.cancelCount);
        assertEquals("Task 5 failure none",0,task5.failureCount);
        assertEquals("Task 5 retry none",0,task5.retryCount);
        
        assertEquals("Task 6 submit once",1,task6.submitCount);
        assertEquals("Task 6 start once",0,task6.startCount);
        assertEquals("Task 6 execute once",0,task6.executeCount);
        assertEquals("Task 6 complete once",0,task6.completeCount);
        assertEquals("Task 6 cancel none",1,task6.cancelCount);
        assertEquals("Task 6 failure none",0,task6.failureCount);
        assertEquals("Task 6 retry none",0,task6.retryCount);
        
        assertEquals("Stage 1 submit once",1,stage.submitCount);
        assertEquals("Stage 1 start once",1,stage.startCount);
        assertEquals("Stage 1 execute once",0,stage.executeCount);
        assertEquals("Stage 1 complete once",1,stage.completeCount);
        assertEquals("Stage 1 cancel none",0,stage.cancelCount);
        assertEquals("Stage 1 failure none",1,stage.failureCount);
        assertEquals("Stage 1 retry none",0,stage.retryCount);
        
        assertEquals("Stage 2 submit once",0,stage2.submitCount);
        assertEquals("Stage 2 start once",0,stage2.startCount);
        assertEquals("Stage 2 execute once",0,stage2.executeCount);
        assertEquals("Stage 2 complete once",0,stage2.completeCount);
        assertEquals("Stage 2 cancel none",1,stage2.cancelCount);
        assertEquals("Stage 2 failure none",0,stage2.failureCount);
        assertEquals("Stage 2 retry none",0,stage2.retryCount);
        
        assertEquals("Task 7 submit once",0,task7.submitCount);
        assertEquals("Task 7 start once",0,task7.startCount);
        assertEquals("Task 7 execute once",0,task7.executeCount);
        assertEquals("Task 7 complete once",0,task7.completeCount);
        assertEquals("Task 7 cancel none",1,task7.cancelCount);
        assertEquals("Task 7 failure none",0,task7.failureCount);
        assertEquals("Task 7 retry none",0,task7.retryCount);
        
        assertEquals("Task 8 submit once",0,task8.submitCount);
        assertEquals("Task 8 start once",0,task8.startCount);
        assertEquals("Task 8 execute once",0,task8.executeCount);
        assertEquals("Task 8 complete once",0,task8.completeCount);
        assertEquals("Task 8 cancel none",1,task8.cancelCount);
        assertEquals("Task 8 failure none",0,task8.failureCount);
        assertEquals("Task 8 retry none",0,task8.retryCount);
        
        assertEquals("Task 9 submit once",0,task9.submitCount);
        assertEquals("Task 9 start once",0,task9.startCount);
        assertEquals("Task 9 execute once",0,task9.executeCount);
        assertEquals("Task 9 complete once",0,task9.completeCount);
        assertEquals("Task 9 cancel none",1,task9.cancelCount);
        assertEquals("Task 9 failure none",0,task9.failureCount);
        assertEquals("Task 9 retry none",0,task9.retryCount);
    }
}
