/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.level3.meanwhile;

import com.level3.meanwhile.base.BaseChain;
import com.level3.meanwhile.state.ClaimCheck;
import com.level3.meanwhile.state.TaskStatus;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for Stage - Staged compositions, execution order, status transitions, etc.
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public class StageTest extends MeanwhileTest {
    
    public StageTest() {
    }
    
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    
    @Override
    @After
    public void tearDown() {
        super.tearDown();
    }

    @Test
    public void testStaged() throws Exception {
        // allow time to grow pool
        stopWatch.block(500);
        final TimerTask task1 = new TimerTask(200L);
        
        final TimerTask task2 = new TimerTask(200L);
        
        final TimerTask task3 = new TimerTask(200L) {
            @Override
            public boolean execute() {
                return false;
            }
        };
        
        final TimerTask task3a = new TimerTask(200L);
        
        final TimerTask task4 = new TimerTask(200L);
        
        final TimerTask task4a = new TimerTask(200L);
        
        final TimerTask task4aa = new TimerTask(200L);
        
        final TimerTask task4ab = new TimerTask(200L) {
            @Override
            public boolean execute() {
                return false;
            }
        };
        
        final TimerTask task4aba = new TimerTask(200L);
        
        final TimerTask task5 = new TimerTask(200L);
        
        final TimerTask task5a = new TimerTask(200L);
        
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(task1);
        tasks.add(task2);
        
        final Stage stage = new Stage(tasks);
        final Stage stage2 = new Stage();
        final Stage stage3 = new Stage();
        
        //execute should just return false
        assertFalse(stage3.execute());
        
        
        
        task3.chain(task3a);
        task4a.chain(task4ab);
        task4a.chain(task4aa);
        task4a.chain(task4ab);
        task4ab.chain(task4aba);
        task4.chain(task4a);
        stage2.stage(task3);
        stage2.stage(task4);
        
        task5.chain(task5a);
        stage3.stage(task5);
        
        stage2.chain(stage3);
        stage.chain(stage2);
        
        Stage returnStage = manager.blockingSubmit(stage);
        
        assertNotNull(returnStage);
        assertEquals("Stage 1 not SUCCESS",TaskStatus.SUCCESS,stage.getStatus());
        assertEquals("Stage 2 not FAILED",TaskStatus.FAILED,stage2.getStatus());
        assertEquals("Stage 3 not CANCELED",TaskStatus.CANCELED,stage3.getStatus());
        assertEquals("Task 1 not SUCCESS",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 2 not SUCCESS",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 4 not SUCCESS",TaskStatus.SUCCESS,task4.getStatus());
        assertEquals("Task 3 not FAILED",TaskStatus.FAILED,task3.getStatus());
        assertEquals("Task 5 not CANCELED",TaskStatus.CANCELED,task5.getStatus());
        assertEquals("Task 5a not CANCELED",TaskStatus.CANCELED,task5a.getStatus());
        assertEquals("Task 3a not FAILED",TaskStatus.CANCELED,task3a.getStatus());
        assertEquals("Task 4a FAILED",TaskStatus.SUCCESS,task4a.getStatus());
        assertEquals("Task 4aa FAILED",TaskStatus.SUCCESS,task4aa.getStatus());
        assertEquals("Task 4ab not FAILED",TaskStatus.FAILED,task4ab.getStatus());
        assertEquals("Task 4aba not FAILED",TaskStatus.CANCELED,task4aba.getStatus());
    }
    
    @Test
    public void testEmptyStagesPass() throws Exception {
        // allow time to grow pool
        Stage stage1 = new Stage();
        Stage stage2 = new Stage();
        Stage stage3 = new Stage();
        
        final SuccessTask task1 = new SuccessTask();
        final SuccessTask task2 = new SuccessTask();
        
        stage3.stage(task1);
        stage2.chain(stage3);
        stage2.chain(task2);
        stage1.chain(stage2);
        
        manager.blockingSubmit(stage1);
        assertEquals("Stage 1 success",TaskStatus.SUCCESS,stage1.getStatus());
        assertEquals("Stage 2 success",TaskStatus.SUCCESS,stage2.getStatus());
        assertEquals("Stage 3 success",TaskStatus.SUCCESS,stage3.getStatus());
        assertEquals("Task 1 success",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task2 2 success",TaskStatus.SUCCESS,task2.getStatus());
    }
    
    @Test
    public void testStageFailOnChainFailure() throws Exception {
        // allow time to grow pool
        Stage stage = new Stage();
        BaseChain task1 = new SuccessTask();
        BaseChain task2 = new FailTask();
        BaseChain task1a = new SuccessTask();
        BaseChain task2a = new SuccessTask();
        
        task1.chain(task1a);
        task2.chain(task2a);
        stage.stage(task1);
        stage.stage(task2);
        
        manager.blockingSubmit(stage);
        
        assertEquals("Stage failed",TaskStatus.FAILED,stage.getStatus());
        assertEquals("Task 2 failed",TaskStatus.FAILED,task2.getStatus());
        assertEquals("Task 2a canceled",TaskStatus.CANCELED,task2a.getStatus());
        assertEquals("Task 1 success",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 1a success",TaskStatus.SUCCESS,task1a.getStatus());
        
        stage = new Stage();
        task1 = new SuccessTask();
        task2 = new SuccessTask();
        task1a = new SuccessTask();
        task2a = new FailTask();
        
        task1.chain(task1a);
        task2.chain(task2a);
        stage.stage(task1);
        stage.stage(task2);
        
        manager.blockingSubmit(stage);
        
        assertEquals("Stage failed",TaskStatus.FAILED,stage.getStatus());
        assertEquals("Task 2 failed",TaskStatus.SUCCESS,task2.getStatus());
        assertEquals("Task 2a canceled",TaskStatus.FAILED,task2a.getStatus());
        assertEquals("Task 1 success",TaskStatus.SUCCESS,task1.getStatus());
        assertEquals("Task 1a success",TaskStatus.SUCCESS,task1a.getStatus());
        
    }
    
    @Test
    public void testCancelStages() throws Exception {
        manager.setMaximumThreadCount(1);
        // allow time to grow pool
        stopWatch.block(500);
        TimerTask task11 = new TimerTask(300L);
        TimerTask task12 = new TimerTask(300L);
        
        TimerTask task21 = new TimerTask(300L);
        TimerTask task22 = new TimerTask(300L);
        
        TimerTask task31 = new TimerTask(300L);
        TimerTask task32 = new TimerTask(300L);
        
        Stage stage1 = new Stage();
        Stage stage2 = new Stage();
        Stage stage3 = new Stage();
        
        stage1.stage(task11);
        stage1.stage(task12);
        
        stage2.stage(task21);
        stage2.stage(task22);
        
        stage3.stage(task31);
        stage3.stage(task32);
        
        stage2.chain(stage3);
        stage1.chain(stage2);
        
        ClaimCheck check = manager.execute(stage1);
        
        stopWatch.block(100);
        manager.cancel(check.toString());
   
        assertEquals(TaskStatus.WORKING,task11.getStatus());
        assertEquals(TaskStatus.CANCELED,task12.getStatus());
        assertEquals(TaskStatus.WORKING,stage1.getStatus());
        
        stopWatch.block(300);        
        assertEquals(TaskStatus.CANCELED,stage2.getStatus());
        assertEquals(TaskStatus.CANCELED,task21.getStatus());
        assertEquals(TaskStatus.CANCELED,task22.getStatus());
        assertEquals(TaskStatus.CANCELED,stage3.getStatus());
        assertEquals(TaskStatus.CANCELED,task31.getStatus());
        assertEquals(TaskStatus.CANCELED,task32.getStatus());
        
        // Block until queued finished
        stopWatch.block(550);
        assertEquals(TaskStatus.SUCCESS,task11.getStatus());
        assertEquals(TaskStatus.CANCELED,task12.getStatus());
        assertEquals(TaskStatus.FAILED,stage1.getStatus());
        
        assertEquals(TaskStatus.CANCELED,stage2.getStatus());
        assertEquals(TaskStatus.CANCELED,task21.getStatus());
        assertEquals(TaskStatus.CANCELED,task22.getStatus());
        assertEquals(TaskStatus.CANCELED,stage3.getStatus());
        assertEquals(TaskStatus.CANCELED,task31.getStatus());
        assertEquals(TaskStatus.CANCELED,task32.getStatus());
        
        // Cancel Stage 2 during Stage 2 using Stage 1 UUID
        task11 = new TimerTask(300L);
        task12 = new TimerTask(300L);
        
        task21 = new TimerTask(300L);
        task22 = new TimerTask(300L);
        
        task31 = new TimerTask(300L);
        task32 = new TimerTask(300L);
        
        stage1 = new Stage();
        stage2 = new Stage();
        stage3 = new Stage();
        
        stage1.stage(task11);
        stage1.stage(task12);
        
        stage2.stage(task21);
        stage2.stage(task22);
        
        stage3.stage(task31);
        stage3.stage(task32);
        
        stage2.chain(stage3);
        stage1.chain(stage2);
        
        check = manager.execute(stage1);
        
        stopWatch.block(650);
        
        
        assertEquals(TaskStatus.SUCCESS,task11.getStatus());
        assertEquals(TaskStatus.SUCCESS,task12.getStatus());
        assertEquals(TaskStatus.SUCCESS,stage1.getStatus());
        
        assertEquals(TaskStatus.WORKING,stage2.getStatus());
        assertEquals(TaskStatus.WORKING,task21.getStatus());
        assertEquals(TaskStatus.QUEUED,task22.getStatus());
        
        assertEquals(TaskStatus.DEQUEUED,stage3.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task31.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task32.getStatus());
        
        manager.cancel(check.toString());
        
        assertEquals(TaskStatus.SUCCESS,task11.getStatus());
        assertEquals(TaskStatus.SUCCESS,task12.getStatus());
        assertEquals(TaskStatus.SUCCESS,stage1.getStatus());
        
        assertEquals(TaskStatus.WORKING,stage2.getStatus());
        assertEquals(TaskStatus.WORKING,task21.getStatus());
        assertEquals(TaskStatus.CANCELED,task22.getStatus());
        
        stopWatch.block(350);   
        assertEquals(TaskStatus.CANCELED,stage3.getStatus());
        assertEquals(TaskStatus.CANCELED,task31.getStatus());
        assertEquals(TaskStatus.CANCELED,task32.getStatus());
        
        stopWatch.block(310);
        
        /*
        // Cancel the stage 2 during stage 1 with Stage 2 UUID
        task11 = new TimerTask(300L);
        task12 = new TimerTask(300L);
        
        task21 = new TimerTask(300L);
        task22 = new TimerTask(300L);
        
        task31 = new TimerTask(300L);
        task32 = new TimerTask(300L);
        
        stage1 = new Stage();
        stage2 = new Stage();
        stage3 = new Stage();
        
        stage1.stage(task11);
        stage1.stage(task12);
        
        stage2.stage(task21);
        stage2.stage(task22);
        
        stage3.stage(task31);
        stage3.stage(task32);
        
        stage2.chain(stage3);
        stage1.chain(stage2);
        
        check = TaskManager.execute(stage1);
        
        stopWatch.block(350);
        assertEquals(TaskStatus.SUCCESS,task11.getStatus());
        assertEquals(TaskStatus.WORKING,task12.getStatus());
        assertEquals(TaskStatus.WORKING,stage1.getStatus());
        
        assertEquals(TaskStatus.DEQUEUED,stage2.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task21.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task22.getStatus());
        
        assertEquals(TaskStatus.DEQUEUED,stage3.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task31.getStatus());
        assertEquals(TaskStatus.DEQUEUED,task32.getStatus());
        
        TaskManager.cancel(stage2.getUUID().toString());
        
        assertEquals(TaskStatus.SUCCESS,task11.getStatus());
        assertEquals(TaskStatus.WORKING,task12.getStatus());
        assertEquals(TaskStatus.WORKING,stage1.getStatus());
        
        assertEquals(TaskStatus.CANCELED,stage2.getStatus());
        assertEquals(TaskStatus.CANCELED,task21.getStatus());
        assertEquals(TaskStatus.CANCELED,task22.getStatus());
        
        assertEquals(TaskStatus.CANCELED,stage3.getStatus());
        assertEquals(TaskStatus.CANCELED,task31.getStatus());
        assertEquals(TaskStatus.CANCELED,task32.getStatus());
        * 
        */
    }
}
