package com.level3.meanwhile;

import com.level3.meanwhile.state.ClaimCheck;
import com.level3.meanwhile.concurrent.MeanwhileFuture;
import com.level3.meanwhile.concurrent.MeanwhileRunner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


/**
 * Static class that creates and manages TaskQueues for use in executing generic asynchronous tasks. 
 * There can be many queues available per JVM, and the number of threads is configurable per TaskQueue
 * 
 * <p>Acts as a static delegate for working with multiple queue instances within a the context of a single application
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.3
 */

public final class TaskQueueManager {
        static {
            initThreadCount();
        }
	private static int defaultNumThreads;
        private static int defaultStageThreads;
        private static Map<String,TaskQueue> queues = new HashMap<String,TaskQueue>();        
	
        /*
         * Resets the Thread Counts to the default, or the number defined by System.properties
         */
        protected static void initThreadCount() {
            defaultNumThreads = System.getProperty("meanwhile.task.threads") != null ? Integer.parseInt(System.getProperty("meanwhile.task.threads")) : 10;
            defaultStageThreads = System.getProperty("meanwhile.stage.threads") != null ? Integer.parseInt(System.getProperty("meanwhile.stage.threads")) : 10;
        }
	
	//Static class, prevent instantiation
	private TaskQueueManager(){};
        
        // Gets a queue by its queue Id String and if it doesn't it exist, it creates one with the default thread counts
        private static TaskQueue getQueue(String queueId) {
            if(queues.containsKey(queueId)) {
                return queues.get(queueId);
            } else {
                createQueue(queueId,defaultNumThreads, defaultStageThreads);
                return queues.get(queueId);
            }
        }
        
        // Stores the queue with the provided queue Id key
        private static void setQueue(String queueId, TaskQueue queue) {
            queues.put(queueId, queue);
        }
        
        // Stores the queue with the provided queue Id key
        private static void removeQueue(String queueId) {
             if(queues.containsKey(queueId)) {
                 queues.remove(queueId);
             }
        }

        // Creates a new queue with the thread size parameters and stores it by the provided queue Id key
        public static void createQueue(String queueId, int threads, int stageThreads) {
            TaskQueue queue = new TaskQueue(threads,stageThreads);
            setQueue(queueId,queue);
        }
        
        /**
	 * Sets the maximum number of threads available in the named TaskQueue for concurrent Task execution. 
         * If the number is lower than the current number of busy threads, the pool will wait until those threads finish 
         * and scale back the number of threads as they are returned to the pool
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param max The maximum number of Tasks that should be executed concurrently. Must be > 0.
	 * @since 0.3
         */
	public static void setMaximumThreadCount(final String queueId, final int max) {
            TaskQueue queue = getQueue(queueId);
            queue.setMaximumThreadCount(max);
	}
        
        /**
	 * Sets the maximum number of threads available in the named TaskQueue for concurrent Task execution
         * If the number is lower than the current number of busy threads, the pool will wait until those threads finish 
         * and scale back the number of threads as they are returned to the pool
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param max The maximum number of Tasks that should be executed concurrently. Must be > 0.
	 * @since 0.3
         */
        public static void setMaximumStagePoolThreadCount(final String queueId, final int max) {
            TaskQueue queue = getQueue(queueId);
            queue.setMaximumStagePoolThreadCount(max);
        }
        
        /**
	 * Adds a task into the executor thread pool of the specified TaskQueue, and returns a Claim Check
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param task The Task to be executed
         * @return a ClaimCheck that may be redeemed to cancel this task
	 * @since 0.3
         * @see Task
         * @see ClaimCheck
         * @see Future
         */
	public static <T extends Task> ClaimCheck execute(String queueId, final T task) {
            return getQueue(queueId).execute(task);
	}
  
        /**
	 * Adds multiple tasks into the executor thread pool, and returns a list of Claim Checks
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param tasks A list of Tasks to be executed
         * @return a List of Futures that wrap the Tasks submitted to the work queue
	 * @since 0.3
         * @see Task
         * @see Future
         */
	public static <T extends Task> List<ClaimCheck> execute(final String queueId, final List<T> tasks) {
             return getQueue(queueId).execute(tasks);
	}
        
        /**
	 * Adds a task into the executor thread pool of the specified TaskQueue, and returns a Future
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param task The Task to be executed
         * @return the completed Task that was executed
	 * @since 0.3
         * @see Task
         * @see Future
         */
	public static <T extends Task> MeanwhileFuture<T> submit(String queueId, final T task) {
		return getQueue(queueId).submit(task);
	}
        
        /**
	 * Adds a task into the executor thread pool of the specified TaskQueue, and returns a Future
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param mr An instance of MeanwhileRunner (which wraps a Task) to be executed
         * @return the completed Task that was executed
	 * @since 0.3
         * @see Task
         * @see Future
         */
	public static <T extends Task> MeanwhileFuture<T> submit(String queueId, final MeanwhileRunner<T> mr) {
                return getQueue(queueId).submit(mr);
	}
        
        /**
	 * Adds multiple tasks into the executor thread pool of the specified TaskQueue, and returns a list of Futures
         * @param queueId The unique String identifier of the backing TaskQueue 
         * @param tasks A list of Tasks to be executed
         * @return a List of Futures that wrap the Tasks submitted to the work queue
	 * @since 0.3
         * @see Task
         * @see Future
         */
	public static <T extends Task> List<MeanwhileFuture<T>> submit(final String queueId, final List<T> tasks) {
            return getQueue(queueId).submit(tasks);
	}
	
	/**
	 * Takes a list of Tasks, submits each one to the Executor pool of the specified TaskQueue, and effectively WAITS until 
	 * all those tasks are completed and then returns a list of the completed tasks in the order they were submitted (i.e., not in the order
	 * they finished)
	 *  
	 * <p>This allows the client to parallelize work in the context of a single synchronous
	 * call.
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param tasks A list of Tasks to be executed
         * @return a List of completed Tasks that were executed
	 * @since 0.3
         * @see Task
         */
	public static <T extends Task> List<T> blockingSubmit(final String queueId, final List<T> tasks) throws Exception {
                return getQueue(queueId).blockingSubmit(tasks);
	}
        
        /**
	 * Takes a single Task, submits it to the Executor pool, and effectively WAITS until 
	 * the task is completed and then returns the Task
	 *  
	 * <p>This allows the client to parallelize work in the context of a single synchronous
	 * call.
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param task The Task to be executed
         * @return the completed Task that was executed
	 * @since 0.3
         * @see Task
         */
	public static <T extends Task> T blockingSubmit(final String queueId, final T task) throws Exception {
                return getQueue(queueId).blockingSubmit(task);
	}
        
        /**
         * 
         * Blocks until the task(s) represented by the ClaimCheck have finished processing. 
         * 
         * <p>This method is very useful for use cases where the client has a ClaimCheck and needs to halt further processing, but
         * is for some reason unable to make an initial blockingSubmit() call. Unit Tests are a good example of this use case.
         * 
         * @return <b>{@code true}</b> if the TaskManager has found the Task represented by the ClaimCheck and successfully waited for
         * execution. A <b>{@code false}</b> is returned if the TaskManager did not find a Task represented by the ClaimCheck and therefore
         * did not actually block (i.e., it returned immediately)
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param claim the ClaimCheck of the Task we wish to block on
         * @since 0.3 
         * @see ClaimCheck
	 */
	public static boolean block(final String queueId, final ClaimCheck claim) throws Exception {
            return getQueue(queueId).block(claim);
	}
        
        /**
         * 
         * Blocks until the task(s) represented by the UUID have finished processing. 
         * 
         * <p>This method is very useful for use cases where the client has a UUID and needs to halt further processing, but
         * is for some reason unable to make an initial blockingSubmit() call. Unit Tests are a good example of this use case.
         * 
         * @return <b>{@code true}</b> if the TaskManager has found the Task represented by the UUID and successfully waited for
         * execution. A <b>{@code false}</b> is returned if the TaskManager did not find a Task represented by the UUID and therefore
         * did not actually block (i.e., it returned immediately)
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param uuid the UUID of the Task we wish to block on
         * @since 0.3 
         * @see ClaimCheck
         * @see UUID
	 */
	public static boolean block(final String queueId, final UUID uuid) throws Exception {
            return getQueue(queueId).block(uuid);
	}
        
        /**
         * 
         * Blocks until the task(s) represented by the UUID String have finished processing. 
         * 
         * <p>This method is very useful for use cases where the client has a UUID String and needs to halt further processing, but
         * is for some reason unable to make an initial blockingSubmit() call. Unit Tests are a good example of this use case.
         * 
         * @return <b>{@code true}</b> if the TaskManager has found the Task represented by the UUID String and successfully waited for
         * execution. A <b>{@code false}</b> is returned if the TaskManager did not find a Task represented by the UUID String and therefore
         * did not actually block (i.e., it returned immediately)
         * @param sUuid the UUID String of the Task we wish to block on
         * @since 0.2.1 
         * @see ClaimCheck
         * @see UUID
	 */
	public static boolean block(final String queueId, final String sUuid) throws Exception {
            return getQueue(queueId).block(sUuid);
	}

        
        /**
         * 
         * Cancels a task that has been scheduled for execution. If task is already in process,
	 * this will do nothing.
         * 
         * <p>Cancel will prevent the execution of all incomplete, not-in-flight Tasks that match the Claim Check,
         * as well as all subtasks that have yet to complete or begin processing.
         * 
         * <p>For instance, consider a scenario with a Chain of Tasks: 
         * 
         * <p>     Task 1 (Complete) -> Task 2 (Working) -> Task 3 (Queued) -> Task 4 (Queued)
         * 
         * <p>Passing in the ClaimCheck for Task 1 (returned from TaskManager.execute()) will result in the cancellation of Task 3 and Task 4.
         * 
         * @return <b>{@code true}</b> if the Task has been canceled, <b>{@code false}</b> if the Task could not be canceled,
         * because it is already in TaskStatus.WORKING, or because it simply does not exist in the queue.
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param claim the ClaimCheck of the task to be canceled
         * @since 0.3 
         * @see Task
	 */
	public static boolean cancel(final String queueId, final ClaimCheck claim) {
            return getQueue(queueId).cancel(claim);
	}
        
        /**
         * 
         * Cancels a queued task that matches the provided UUID. If task is already in process, this will do nothing.
         * 
          * <p>Cancel will prevent the execution of all incomplete, not-in-flight Tasks that match the UUID,
         * as well as all subtasks that have yet to complete or begin processing.
         * 
         * @return <b>{@code true}</b> if the Task has been canceled, <b>{@code false}</b> if the Task could not be canceled,
         * because it is already in TaskStatus.WORKING, or because it simply does not exist in the queue. 
         * @param queueId The unique String identifier of the backing TaskQueue
	 * @param uuid the UUID of the Task to be canceled
         * @since 0.3 
         * @see UUID
	 */
	public static boolean cancel(final String queueId, final UUID uuid) {
            return getQueue(queueId).cancel(uuid);
	}
        
        /**
         * Cancels a queued task that matches the provided UUID string. If task is already in process,
	 * this will do nothing.
         * 
         * <p>Cancel will prevent the execution of all incomplete, not-in-flight Tasks that match the UUID string,
         * as well as all subtasks that have yet to complete or begin processing.
         * 
         * @return <b>{@code true}</b> if the Task has been canceled, <b>{@code false}</b> if the Task could not be canceled,
         * because it is already in TaskStatus.WORKING, or because it simply does not exist in the queue.
         * @param queueId The unique String identifier of the backing TaskQueue
	 * @param uuid a String representing the UUID of the Task
         * @since 0.3 
         * @see UUID#toString
	 */
	public static boolean cancel(final String queueId, final String uuid) {
            return getQueue(queueId).cancel(uuid);
	}
        
	/**
	 * Returns the current queue size of the specified TaskQueue. This number represents the number of Tasks currently <i>waiting</i> to be
         * executed, and not the number of Tasks currently being executed.
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return the current size of the Task work queue
	 * @since 0.3
         */
	public static int getQueueSize(final String queueId) {
                return getQueue(queueId).getQueueSize();
	}
        
        /**
	 * Returns the current stage queue size of the specified TaskQueue. This number represents the number of Tasks currently <i>waiting</i> to be
         * executed, and not the number of Tasks currently being executed.
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return the current size of the Task work queue
	 * @since 0.3
         */
	public static int getStageQueueSize(final String queueId) {
                return getQueue(queueId).getStageQueueSize();
	}
        
        /**
	 * Returns the maximum number of threads that are available for concurrent execution of Tasks in the specified TaskQueue. This is not reflective of the
         * current number of Tasks currently in WORKING state.
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return the maximum number of Tasks that may be executed concurrently
	 * @since 0.3
         */
	public static int getMaximumPoolSize(final String queueId) {
                return getQueue(queueId).getMaximumPoolSize();
	}
        
        /**
	 * Returns the current number of threads that are busy executing Tasks in the specified TaskQueue. 
         * @return the number of threads that are currently busy executing tasks
         * @param queueId The unique String identifier of the backing TaskQueue
	 * @since 0.3
         */
	public static int getActiveCount(final String queueId) {
             return getQueue(queueId).getActiveCount();   
	}
        
        /**
	 * Gets a list of all of the queued Tasks in the specified TaskQueue
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return a List of ClaimChecks for all of the queued Tasks
	 * @since 0.3
         * @see ClaimCheck
         */
	public static List<ClaimCheck> getQueuedTasks(final String queueId) {
            return getQueue(queueId).getQueuedTasks();
	}
        
        /**
	 * Gets a list of all of the Tasks currently in-flight in the specified TaskQueue
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return a List of ClaimChecks for all of the active  Tasks
	 * @since 0.3
         * @see ClaimCheck
         */
	public static List<ClaimCheck> getActiveTasks(final String queueId) {
            return getQueue(queueId).getActiveTasks();
	}
	
	/**
	 * Reports whether the specified Task is queued for execution in the specified TaskQueue
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return <b>{@code true}</b> if the Task has been queued, <b>{@code false}</b> otherwise
	 * @since 0.3
         * @see Task
         */
	public static boolean isQueued(final String queueId, final Object claimCheck) {
            return getQueue(queueId).isQueued(claimCheck);
	}
	/**
	 * Reports the current position of the specified task within the specified TaskTask
	 * returns -1 if not in the queue
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return the index of the Task within the work queue, -1 if not found
	 * @since 0.3
         * @see Task
         */
	public static int getQueuePosition(final String queueId, final Object claimCheck) {
		return getQueue(queueId).getQueuePosition(claimCheck);
	}        
        
        /**
	 * Shuts down the Executor pool of all of the TaskQueues immediately
         * @param queueId The unique String identifier of the backing TaskQueue
	 * @since 0.3
         */
	public static void shutdown() {
            for(TaskQueue queue : queues.values()) {
                queue.shutdown();
            }
            queues.clear();
	}
        
	/**
	 * Shuts down the Executor pool of the specified TaskQueue immediately
         * @param queueId The unique String identifier of the backing TaskQueue
	 * @since 0.3
         */
	public static void shutdown(final String queueId) {
            getQueue(queueId).shutdown();
            removeQueue(queueId);
	}
	
	/**
	 * Shuts down the Executor pool of the specified TaskQueue, specifying the amount of time to wait for tasks to finish
         * @param queueId The unique String identifier of the backing TaskQueue
         * @param millisToWait The amount of time to wait for tasks to complete before forcing shutdown of the pool
	 * @since 0.1
         */
	public static void shutdown(final String queueId, final long millisToWait) {
            getQueue(queueId).shutdown(millisToWait);
            removeQueue(queueId);
        }    
        
        /**
         * Returns the status of the pool of the specified TaskQueue.
         * 
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return true if the pool has started, false if it is shutdown
         * @since 0.3
         */
        public static boolean isPoolStarted(final String queueId) {
            return getQueue(queueId).isPoolStarted();
        }
        
        /**
         * Returns the status of the stage pool of the specified TaskQueue.
         *
         * @param queueId The unique String identifier of the backing TaskQueue
         * @return true if the stage pool has started, false if it is shutdown
         * @since 0.3
         */
        public static boolean isStagePoolStarted(final String queueId) {
            return getQueue(queueId).isStagePoolStarted();
        }
}