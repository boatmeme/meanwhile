package com.level3.meanwhile;

import java.util.UUID;

/**
 * Encapsulates a discrete unit of work meant to be run  by TaskManager
 * 
 * <p>Class is abstract and implements Runnable. Clients must provide a concrete implementation of the execute() method.
 * Usually, a client would extend this class, but you may at a minimum create an Anonymous Inner Class, and @Override the 
 * execute() method as a quick-start.
 * 
 * <p>We use this instead of a raw Runnable so that we may enrich the functionality of the thread pool / work queue
 * by adding things like comparators (for prioritization) and overriding {@code equals()} and {@code hashCode()} (for
 * determining existence and position in the work queue)
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public interface Task {
    
   /**
     * Abstract {@code execute()} method. <b>All subclasses of Task must provide an implementation of {@code execute()}</b>. 
     * 
     * <p>This is where a client application will perform most of its work. 
     * 
     * <p>The specific conditions that constitute a Task failure or Task success are entirely at the discretion of the implementor.
     * 
     * <p>In the case of Task failure, return false. In the case of Task success return true.
     * 
     * @return <b>{@code true}</b> to indicate success, <b>{@code false}</b> to indicate failure
     * @since 0.1
     */
    public boolean execute();
    
    /**
    * This method is called after the task has been queued for execution by the TaskManager
    * @since 0.1
    */
    public void onSubmit();

    /**
     * This method is called immediately prior to the execute() method
     * @since 0.1
     */
    public void onStart();

    /**
     * This method is the last method called in the order of execution. It is executed regardless of the success or failure of the task.
     * @since 0.1
     */
    public void onComplete();
    
    /**
     * This method is the executed after execute(), if execute() returns TRUE (indicating success)
     * @since 0.1
     */
    public void onSuccess();
    
    /**
     * This method is the executed after execute(), if execute() returns FALSE (indicating failure)
     * @since 0.1
     */
    public void onFailure();
    
    /**
     * This method is the executed if the task has been removed from TaskManager's work queue. 
     * @since 0.1
     */
    public void onCancel();
    
    /**
     * This method is executed prior to the next call of onStart(), if the task returns false from execute(), followed by true from retryOnFail().
     * 
     * <p>This implementation is where clients should do their bookkeeping surrounding retry attempts.
     * 
     * <p>For instance, if the implementation wants to increment a counter every time a Task is retried, this is where you would perform that work.
     * 
     * @since 0.1
     */
    public void onRetry(); 
    
    /**
     * If this method returns true, then the task is executed again if it FAILS (i.e., the Task's execute() method returns FALSE).
     * 
     * <p>Should you need to re-run your implementation of Task (in the event of resource contention, for example), 
     * this method should return true.
     * 
     * <p>It is good practice for clients that return true from retryOnFail() to build-in an escape hatch by keeping track of a maximum
     * number of retries (by incrementing a counter in onRetry() for example). 
     * 
     * <p>Such a strategy would work well to prevent infinite loops wherein a failing Task perpetually retries itself
     * 
     * @since 0.1
     */
    public boolean retryOnFail();
    
    /**
     * This method must return a UUID (Universally Unique Identifier) representing a unique instance of a Task.
     * BaseTask provides an implementation of this that generates UUIDs based on a Pseudo-Random Number Generator {@code UUID.randomUUID()}
     * @see java.util.UUID
     * @see com.level3.meanwhile.base.BaseTask#getUUID
     * @see com.level3.meanwhile.state.ClaimCheck
     * @since 0.2
     */
    public UUID getUUID();
    
}
