/**
 * The main interfaces for using the Meanwhile library.
 * 
 * <p>Meanwhile is a small Java library for performing arbitrary work concurrently. Configuration is minimal (one line), 
 * and creation of Tasks requires no more than the implementation of a single execute() method.
 *
 * <p>There are four basic constructs with which users should familiarize themselves; The <b>{@code Task}</b>. 
 * The <b>{@code Chain}</b>. The <b>{@code Stage}</b>. And the The <b>{@code TaskManager}</b>.
 *
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
package com.level3.meanwhile;
