/**
 * Classes used for representing the state of Tasks
 *
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.2
 */
package com.level3.meanwhile.state;
