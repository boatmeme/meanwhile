package com.level3.meanwhile.state;

import java.util.UUID;

/**
 * Used by clients to manage Tasks with the TaskManager.
 * 
 * <P>TaskManager requires a ClaimCheck to cancel a task and to report on it's position within the queue.
 * 
 * <p>A ClaimCheck represents a single task or a composite set of Tasks (in the instance of Chain and Stage tasks).
 * 
 * <p>Should a client application need to persist a ClaimCheck, it should store the output of toString(). This UUID can later be
 * used to cancel or check on status of a task with the TaskManager
 * 
 * @see com.level3.meanwhile.TaskManager#cancel
 * @see com.level3.meanwhile.TaskManager#getQueuePosition 
 * @see com.level3.meanwhile.TaskManager#isQueued
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.2
 */
public class ClaimCheck {
    private UUID uuid;
    private ClaimCheck parent;
    
    public ClaimCheck(UUID uuid) {
        this.uuid = uuid;
    }
    
    public ClaimCheck(UUID uuid, final ClaimCheck parent) {
        this.uuid = uuid;
        this.parent = parent;
    }
    
    public ClaimCheck(final String uuidString) {
        uuid = UUID.fromString(uuidString);
    } 
    
    @Override
    public String toString() {
        return uuid.toString();
    }
    
    /**
    * Returns the hashCode of the Task, which is just the {@code hashCode()} of its UUID
    * @since 0.1
    */
    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    /**
     * Equals depends on the UUID - in fact, you can pass a UUID (obj or String) and it can equal the Task object
     * 
     * @param obj a Task, UUID, or String representing a UUID of this task
     * @return <b>{@code true}</b> if this object equals the parameter Object, <b>{@code false}</b> if otherwise
     * @see Object
     * @since 0.1
     */
    @Override
    public boolean equals(final Object obj) {
        boolean isEqual = false;
        if (this == obj) {
            isEqual = true;
        } else if (obj instanceof UUID) {
            isEqual = uuid.equals(obj);
        } else if (obj instanceof String) {
            isEqual = uuid.equals(UUID.fromString((String)obj));
        } else if (!(obj instanceof ClaimCheck)) {
            return false;
        } else if(this.uuid.equals(((ClaimCheck) obj).uuid)) {
            isEqual = true;
        }
        if(!isEqual && parent!=null) {
            isEqual = parent.equals(obj);
        }
        return isEqual;
    }
}
