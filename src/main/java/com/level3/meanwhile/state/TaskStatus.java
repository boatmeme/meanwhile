package com.level3.meanwhile.state;

/**
 *
 * An Enum representing the current, internal status of a Task. Used by the BaseTask to represent its Status, this enum
 * may also be used to model alternate status transitions within client implementations.
 * 
 *   <ul>
 *   <li><B>DEQUEUED</B> - Task has not yet been submitted to the TaskManager's work queue via submit() or blockingSubmit()
 *   <li><B>QUEUED</B> - Task has been queued for execution via the TaskManager's submit() or blockingSubmit()
 *   <li><B>WORKING</B> - Task is in the middle of processing its execute() method
 *   <li><B>FAILED</B> - Task has failed
 *   <li><B>SUCCESS</B>- Task has completed successfully
 *   <li><B>CANCELED</B> - Task has been canceled, either or explicitly or because a task or stage that it is dependent upon has been canceled
 *   <li><B>RETRY</B> - Task has been flagged for re-execution
 *   </ul>
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public enum TaskStatus {
    /**
     * Indicates that a Task has not yet been submitted to the TaskManager's work queue via {@code submit()} or {@code blockingSubmit()}
     */
    DEQUEUED,
    /**
     * Indicates that a Task has been queued for execution via the TaskManager's {@code submit()} or {@code blockingSubmit()}
     */
    QUEUED,
    /**
     * Indicates that a Task is in the middle of processing its {@code execute()} method
     */
    WORKING,
    /**
     * Indicates that a Task has failed
     */
    FAILED,
    /**
     * Indicates that a Task has completed successfully
     */
    SUCCESS,
    /**
     * Indicates that a Task has been canceled, either or explicitly or because a task or stage that it is dependent upon has been canceled
     */
    CANCELED,
    /**
     * Indicates that a Task has been flagged for re-execution
     */
    RETRY
}
