package com.level3.meanwhile.concurrent;

import com.level3.meanwhile.Task;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


/**
 * ThreadPoolExecutor that performs additional functionality, including keeping track of in-flight Futures so that
 * we may cancel tasks that are technically not queued yet (i.e., Tasks that are dependent upon in-flight tasks 
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public final class MeanwhileThreadPoolExecutor extends ThreadPoolExecutor {
    private ConcurrentMap<Object, MeanwhileFuture> inFlight;
    
    public MeanwhileThreadPoolExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit, final BlockingQueue<Runnable> workQueue) { 
        super(corePoolSize,maximumPoolSize,keepAliveTime,unit,workQueue); 
        inFlight = new ConcurrentHashMap<Object, MeanwhileFuture>(corePoolSize,corePoolSize,corePoolSize);
    }
    
    @Override
    protected void beforeExecute(final Thread t, final Runnable r) {
     super.beforeExecute(t, r);
     if(r instanceof MeanwhileFuture) {
         MeanwhileFuture future = (MeanwhileFuture)r;
         inFlight.putIfAbsent(future.getClaimCheck(), future);
     }
   }
    
    @Override
    protected void afterExecute(final Runnable r, final Throwable t) {
        super.afterExecute(r,t);
        if(r instanceof MeanwhileFuture) {
         MeanwhileFuture future = (MeanwhileFuture)r;
         inFlight.remove(future.getClaimCheck());
        }
    }

    public List<MeanwhileFuture> getActiveFutures() {
        List<MeanwhileFuture> list = new ArrayList<MeanwhileFuture>();
        list.addAll(inFlight.values());
        return list;
    }
    
    /**
    * <p>Cancels a task that has been scheduled for execution. If task is already in process, this method will do nothing.
    * <p> Also checks if tasks have been "pseudo-queued", meaning they have not TECHNICALLY been queued at the Java Thread
    * level, rather they have been queued by virtue of the fact that a task upon which the "pseudo-queued" Task is dependent
    * is currently in-flight at the Java Thread level.
    * 
    * @param o A task, UUID, or UUID String representing the task to be canceled
    * @return <b>{@code true}</b> if the Task has been canceled, <b>{@code false}</b> if the Task could not be canceled,
    * because it is already in TaskStatus.WORKING, or because it simply does not exist in the queue.
    * @since 0.1
    */
     public boolean cancel(final Object o) {
        boolean canceled = false;
        for(MeanwhileFuture future : inFlight.values()) {
                if(future.equals(o)) {
                    if(future.cancel(false)) {
                        canceled = true;
                    }
                }
        }
        for(Runnable r : getQueue()) {
            if(r instanceof MeanwhileFuture) {
                MeanwhileFuture future = (MeanwhileFuture)r;
                if(future.equals(o)) {
                    if(future.cancel(false)) {
                        canceled = true;
                    }
                }
            }
        }
        return canceled;
    }
     
    /**
    * <p>Returns the MeanwhileFuture that corresponds to the ClaimCheck, UUID, or UUID String passed in as a parameter
    * 
    * @param o A ClaimCheck, UUID, or UUID String representing the Task wrapped by a MeanwhileFuture
    * @return <b>{@code MeanwhileFuture}</b> if the Task matching the ClaimCheck is in flight, return that MeanwhileFuture. If not, return a matching MeanwhileFuture
    * that is queued. If no matching MeanwhileFuture is found, <b>{@code null}</b> is returned.
    * @since 0.2.1
    */
     public <T extends Task> MeanwhileFuture<T> getFuture(final Object o) {
        for(MeanwhileFuture iFuture : inFlight.values()) {
                if(iFuture.equals(o)) {
                   return iFuture;
                }
        }
        for(Runnable r : getQueue()) {
            if(r instanceof MeanwhileFuture) {
                MeanwhileFuture rFuture = (MeanwhileFuture)r;
                if(rFuture.equals(o)) {
                    return rFuture;
                }
            }
        }
        return null;
    }
}
