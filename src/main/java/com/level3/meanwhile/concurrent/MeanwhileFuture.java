package com.level3.meanwhile.concurrent;

import com.level3.meanwhile.state.ClaimCheck;
import com.level3.meanwhile.Task;
import com.level3.meanwhile.state.TaskStatus;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Used by TaskManager to wrap the MeanwhileRunner (which in turn wraps the Task). 
 * 
 * We want to wrap the tasks because it makes queue management easier, particularly the canceling of queued tasks.
 * 
 * <p>This class is final and the constructor is package-private which implies that it is only for use by this framework and not
 * to be extended or instantiated by client applications.
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.1
 */
public final class MeanwhileFuture<T extends Task> extends FutureTask {
    
    private MeanwhileRunner<T> runner;
    
    /**
    * @since 0.1
    */
    public MeanwhileFuture(final MeanwhileRunner mr) {
        super(mr,mr.getTask());
        this.runner = mr;
    }
    
    @Override
    public T get() throws ExecutionException,InterruptedException {
        T returnObj = (T)super.get();
        for(MeanwhileFuture<T> future : runner.getChainedFutures()) {
            future.get();
        }
        return returnObj;
    }
    
    /**
    * 
    * Gets the ClaimCheck of the wrapped task
    * 
    * @return ClaimCheck representing this Task
    * @since 0.1
    */
    public ClaimCheck getClaimCheck() {
        return runner.getClaimCheck();
    }
    
    /**
    * 
    * Cancels a Task
    * 
    * @param interrupt a boolean indicating whether or not to attempt to cancel an in-flight Task
    * 
    * @return <b>{@code true}</b> if the Task is successfully CANCELED
    * @since 0.1
    */
    @Override
    public boolean cancel(final boolean interrupt) {
        boolean canceled = false;
        if(!TaskStatus.WORKING.equals(runner.getStatus()) && super.cancel(interrupt)) {
            canceled = true;
        }
        if(runner.cancel()) {
            canceled = true;
        }
        return canceled;
    }
    
    
    public boolean isSuccess() {
        return runner.isSuccess();
    }
    
    /**
    * Returns the hashCode of the MeanwhileRunner
    * 
    * @see MeanwhileRunner#equals
    * @since 0.1
    */
    @Override
    public int hashCode() {
        return runner.hashCode();
    }

    /**
     * Equals depends on the Task's MeanwhileRunner - in fact, you can pass a ClaimCheck (or UUID obj or UUID String) and it can equal the Task object
     * 
     * @see MeanwhileRunner#equals
     * @since 0.1
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;        
        return runner.equals(obj);
    }
    
}
