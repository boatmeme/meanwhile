package com.level3.meanwhile;

import java.util.List;

/**
 * Defines a Task that has dependent Tasks that are intended to be run upon successful completion of this task
 * 
 * <p>Interface extends Task. Clients must provide a concrete implementation of the execute() method as well as all Chain-related methods.
 * 
 * If a client application wishes to inherit some default behavior for a Chain, it should extend BaseChain.
 * 
 * @see Task
 * @see com.level3.meanwhile.base.BaseChain
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.2
 */
public interface Chain extends Task {
    
    /**
     * Get the list of tasks chained to this one, and to be executed after this one
     * 
     * @return a list of the Tasks to be executed upon success of this one
     * @since 0.1
     */
    public List<Task> getChainedTasks();
    
}
