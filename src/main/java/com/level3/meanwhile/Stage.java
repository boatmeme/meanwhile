package com.level3.meanwhile;

import com.level3.meanwhile.base.BaseChain;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Encapsulates a series of related Tasks intended to be run by the TaskManager as a single, composite unit of work. 
 * 
 * <p>Provides a concrete implementation of the BaseChain abstract class
 * 
 * <p>All tasks added to the Stage - via {@code addToStage()} or passed in on the Constructor - must be completed before the Stage is 
 * considered complete. 
 * 
 * <p>Only after all Tasks in the Stage have completed with status of SUCCESS, will the next series of Chained Tasks be queued for 
 * execution. If any task within the Stage has FAILED, then the Stage itself will be considered FAILED and all subsequent, chained tasks
 * will be CANCELED.
 * 
 * <p>Stage is a special case of Task that blocks on the completion of its staged tasks, and therefore it is handled 
 * by its own queue inside of the TaskManager.
 * 
 * @see Task
 * @see BaseChain
 * @see com.level3.meanwhile.state.TaskStatus
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.2
 */
public class Stage extends BaseChain {
    private List<Task> stageTasks = new CopyOnWriteArrayList<Task>(); 
    
    /** 
     * Constructor specifying a list of Tasks that should comprise the Stage. The Stage will not be considered complete until
     * all tasks comprising the stage have been completed.
     * 
     * @param tasks a List of Tasks that will comprise the Stage
     * @see Task
     * @since 0.1 
     */ 
    public Stage(final List<Task> tasks) {
        stageTasks.addAll(tasks);
    }
    
    /** 
     * Default constructor for Stage 
     * @since 0.1
    */ 
    public Stage() {
        
    }
    
    /** 
     * 
     * Adds a new Task to the Stage to be executed in parallel with the other Tasks that are members of this Stage
     * 
     * @param task a Tasks that will be added to the list of Tasks that comprise the Stage
     * @see Task
     * @since 0.1
    */ 
    public void stage(final Task task) {
        stageTasks.add(task);
    }
    
    /** 
     * 
     * Gets the list of all Tasks that comprise this Stage
     * 
     * @return a List of Tasks that comprise the Stage
     * @see Task
     * @since 0.1
    */
    public List<? extends Task> getStagedTasks() {
        return stageTasks;
    }
    
    /**
     * This is a no-op that always returns false for Stage. Internally, the TaskManager blocks on execution of all Staged Tasks.
     * 
     * <p>You may extends Stage in order to @Override the Lifecycle method behaviors, but you may not @Override execute() as the 
     * behavior for a Stage is set in stone.
     * 
     * @since 0.1
     */
    @Override
    public final boolean execute() {
        return false;
    }
    
    /**
     * Called when the Stage is queued for execution. Does not call onSubmit() of any Staged Tasks or Chained Tasks (via super.onSubmit())
     * 
     * @see BaseChain#onSubmit
     * @since 0.1
     */
    @Override
    public void onSubmit() {
        super.onSubmit();
    }
    /**
     * Called when the Stage is canceled for execution. Calls onCancel() of all Staged Tasks, and all Chained Tasks (via super.onCancel())
     * 
     * @see BaseChain#onCancel
     * @see TaskManager#cancel
     * @since 0.1
     */
    @Override
    public void onCancel() {
        super.onCancel();
        for(Task task : stageTasks) {
            task.onCancel();
        }
    }
}
