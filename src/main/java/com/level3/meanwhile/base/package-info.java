/**
 * Abstract base classes that are useful as a starting point for client implementations
 * 
 * <p>Extend the <b>{@code BaseTask}</b> to inherit basic status transitions.
 * <p>Extend the <b>{@code BaseChain}</b> to inherit the ability to Chain dependent tasks together.
 *
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.2
 */
package com.level3.meanwhile.base;
