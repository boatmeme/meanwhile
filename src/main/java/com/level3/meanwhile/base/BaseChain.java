package com.level3.meanwhile.base;

import com.level3.meanwhile.Chain;
import com.level3.meanwhile.Task;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Encapsulates a Task that has dependent Tasks that are intended to be run upon <b>successful</b> completion of this task
 * 
 * <p>This abstract Class extends BaseTask (inheriting its TaskStatus mechanism) and implements Chain,
 *    providing a basic implementation of Chain functionality. 
 * 
 * <p>Clients must provide a concrete implementation of the execute() method.
 * 
 * <p>Usually, a client would extend this class, but you may at a minimum create an Anonymous Inner Class, and @Override the 
 * execute() method as a quick-start.
 * 
 * <p>The BaseChain class implements the following behavior:
 * <ul>
 *     <li>{@code onCancel()} - cancels all chained tasks</li>
 *     <li>{@code onFailure()} - cancels all chained tasks</li>
 *     <li>{@code onSubmit()} - calls onSubmit() of all chained tasks</li>     
 * </ul>
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.2
 */
public abstract class BaseChain extends BaseTask implements Chain {
    
    /**
     * The list of chained tasks
     */
    private final List<Task> chainedTasks = new CopyOnWriteArrayList<Task>();
    
    /**
     * Adds a new Task to be executed upon Success of this task
     * 
     * Internally, the new task is simply added to the existing list of chained tasks returned by {@code getChainedTasks()}
     * 
     * @param nextTask a new Task to execute upon successful completion of this Task instance
     * @see BaseChain#getChainedTasks 
     * @since 0.1
     */
    public void chain(final Task nextTask) {
        if(nextTask!=null) {
            chainedTasks.add(nextTask);
        }
    }
    
    /**
     * Adds a list of new Tasks to be executed upon Success of this task
     * 
     * Internally, the new tasks are simply added to the existing list of chained tasks returned by {@code getChainedTasks()}
     * 
     * @param nextTasks a List of Tasks to execute upon successful completion of this Task instance
     * @see BaseChain#getChainedTasks 
     * @since 0.1
     */
    public void chain(final List<Task> nextTasks) {
        if(nextTasks!=null) {
            chainedTasks.addAll(nextTasks);
        }
    }
    
    /**
     * Inserts a new BaseChain into the middle of an existing chain. Calling this method removes the chained tasks from 
     * this BaseChain instance, adds those chained tasks to the nextTask passed in as a parameter, and then adds the nextTask
     * as the first and only task in this Task instance's chained tasks.
     * 
     * If that description is too much to parse, imagine breaking a physical chain to insert an additional link at the
     * point of breakage and you've understood the purpose of {@code insertLink}
     * 
     * @param nextTask a new BaseChain to insert as a link into this chain
     * @see BaseChain#getChainedTasks 
     * @since 0.2
     */
    public void insertLink(final BaseChain nextTask) {
        synchronized(chainedTasks) {
            nextTask.setChainedTasks(this.getChainedTasks());
            this.clearChainedTasks();
            this.chain(nextTask);
        }
    }
    
    /**
     * Get the list of tasks chained to this one
     * 
     * @return a list of the Tasks to be executed upon success of this one
     * @since 0.1
     */
    public List<Task> getChainedTasks() {
        return this.chainedTasks;
    }
    
    /**
     * Set the list of tasks chained to this one
     * 
     * @param tasks a list of the Tasks to be executed upon success of this one
     * @since 0.1
     */
    public void setChainedTasks(final List<Task> tasks) {
        synchronized(chainedTasks) {
           this.clearChainedTasks();
           this.chainedTasks.addAll(tasks); 
        }
    }
    
    /**
     * Removes all Tasks Chained to this Task
     * 
     * @since 0.1
     */
    public void clearChainedTasks() {
        chainedTasks.clear();
    }
    
    /**
     * Called when the Task is queued for execution by the TaskManager. Does not call onSubmit() of any Chained tasks.
     * 
     * @see com.level3.meanwhile.TaskManager#submit
     * @see com.level3.meanwhile.TaskManager#execute
     * @see com.level3.meanwhile.TaskManager#blockingSubmit
     * @since 0.1
     */
    @Override
    public void onSubmit() {
        super.onSubmit();
    }
    
    /**
     * Called when the Task is canceled for execution. Calls onCancel() of all Chained Tasks
     * 
     * @see com.level3.meanwhile.TaskManager#cancel
     * @since 0.1
     */
    @Override
    public void onCancel() {
        super.onCancel();
        for(Task task : this.getChainedTasks()) {
            task.onCancel();
        }
    }
    
     /**
     * Called if the Task's {@code execute()} method returns false. Calls onCancel() of all Chained Tasks
     * 
     * @see Task#execute()
     * @since 0.1
     */
    @Override
    public void onFailure() {
        super.onFailure();
        for(Task task : this.getChainedTasks()) {
            task.onCancel();
        }
    }
}
