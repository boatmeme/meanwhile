package com.level3.meanwhile.base;

import com.level3.meanwhile.Task;
import com.level3.meanwhile.state.TaskStatus;
import java.util.UUID;

/**
 * Encapsulates a discrete unit of work meant to be run  by TaskManager
 * 
 * <p>Class is abstract and implements Task. Clients must provide a concrete implementation of the execute() method.
 * Usually, a client would extend this class, but you may at a minimum create an Anonymous Inner Class, and @Override the 
 * execute() method as a quick-start.
 * 
 * 
 * @author Jonathan Griggs <Jonathan.Griggs@Level3.com>
 * @since 0.2
 */
abstract public class BaseTask implements Task {
    /**
     * The default status of a Task is DEQUEUED
     * @see TaskStatus
     */
    protected TaskStatus status = TaskStatus.DEQUEUED;
    private UUID uuid = UUID.randomUUID();
    
   /**
     * Abstract {@code execute()} method. <b>All subclasses of Task must provide an implementation of {@code execute()}</b>. 
     * 
     * <p>This is where a client application will perform the entirety of its work. 
     * 
     * <p>Clients are responsible for determining whether a particular instance of Task has succeeded or failed.
     * 
     * <p>In the case of Task failure, return false. In the case of Task success return true.
     * 
     * @return <b>{@code true}</b> to indicate success, <b>{@code false}</b> to indicate failure
     * @since 0.1
     */
    public abstract boolean execute();
    
    /**
    * This method is called after the task has been queued for execution by the TaskManager
    * <p><i><b>Note:</b></i> All subclasses of Task should probably call {@code super.onSubmit()} from within the @Override implementation</i></p>
    * @since 0.1
    */
    public void onSubmit() {
        status = TaskStatus.QUEUED;
    }

    /**
     * This method is called immediately prior to the execute() method
     * <p><i><b>Note:</b></i> All subclasses of Task should probably call {@code super.onStart()} from within the @Override implementation</i></p>
     * @since 0.1
     */
    public void onStart() {
        status = TaskStatus.WORKING;
    }
    
    /**
     * This method is called immediately prior to the execute() method
     * <p><i><b>Note:</b></i> All subclasses of Task should probably call {@code super.onStart()} from within the @Override implementation</i></p>
     * @since 0.1
     */
    public void onRetry() {
        status = TaskStatus.RETRY;
    }
    
    /**
     * The BaseTask implementation of this method always returns false.
     * 
     * <p>Should you need to re-run your implementation of Task (in the event of resource contention, for example), 
     * you should extend BaseTask and override this method to (conditionally) return true;
     * 
     * <p>It is a good idea for subclasses that return true from retryOnFail() to build-in an escape hatch by keeping track of a maximum
     * number of retries (by incrementing a counter attribute in onRetry(), for example). 
     * 
     * <p>Such a strategy would work well to prevent infinite loops wherein a failing Task perpetually retries itself
     * 
     * @since 0.1
     */
    public boolean retryOnFail() {
        return false;
    }

    /**
     * This method is the last method called in the order of execution. It is executed regardless
     * of the success or failure of the task.
     * <p><i><b>Note:</b></i> All subclasses of Task should probably call {@code super.onComplete()} from within the @Override implementation</i></p>
     * @since 0.1
     */
    public void onComplete() {
        //No-op
    }
    
    /**
     * This method is the executed after execute(), if execute() returns TRUE (indicating success)
     * <p><i><b>Note:</b></i> All subclasses of Task should probably call {@code super.onSuccess()} from within the @Override implementation</i></p>
     * @since 0.1
     */
    public void onSuccess() {
        status = TaskStatus.SUCCESS;
    }
    
    /**
     * This method is the executed after execute(), if execute() returns FALSE (indicating failure)
     * <p><i><b>Note:</b></i> All subclasses of Task should probably call {@code super.onFailure()} from within the @Override implementation</i></p>
     * @since 0.1
     */
    public void onFailure() {
        status = TaskStatus.FAILED;
    }
    
    
    /**
     * This method is the executed if the task has been removed from TaskManager's work queue. 
     * <p><i><b>Note:</b></i> All subclasses of Task should probably call {@code super.onCancel()} from within the @Override implementation</i></p>
     * @since 0.1
     */
    public void onCancel() {
        status = TaskStatus.CANCELED;
    }

    /**
     * Reports the current status of the Task
     * 
     * @return <b>{@code TaskStatus}</b>, an Enum representing the current status of the task
     * @see TaskStatus
     * @since 0.1
     */
    public TaskStatus getStatus() {
        return status;
    }    
    
    /**
     * Returns a Pseudo-Randomly Generated UUID for use in uniquely identifying this Task
     * 
     * @return <b>{@code UUID}</b>, a Universally Unique Identifier
     * @see java.util.UUID
     * @see Task#getUUID() 
     * @since 0.2
     */
    public UUID getUUID() {
        return uuid;
    } 
}
